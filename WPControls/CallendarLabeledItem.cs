﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPControls
{
    public class CalendarLabeledItem : CalendarItem
    {
        public CalendarLabeledItem(Calendar owner)
            : base(owner)
        {

        }

        /// <summary>
        /// Day number for this calendar cell.
        /// This changes depending on the month shown
        /// </summary>
        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        /// <summary>
        /// Day number for this calendar cell.
        /// This changes depending on the month shown
        /// </summary>
        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(CalendarLabeledItem), new PropertyMetadata("", OnLabelChanged));

        private static void OnLabelChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            var item = source as CalendarLabeledItem;
            if (item != null)
            {
                item.SetForecolor();
                item.SetBackcolor();
            }
        }
    }
}
