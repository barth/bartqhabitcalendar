﻿using BartqHabitCallendar.Model;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using BartqHabitCallendar.Code;

namespace BartqHabitCallendar.ViewModels
{
    public class CommitmentViewModel : PropertyChangingBase
    {
        public delegate void ModelDeletedEventHandler(object sender, EventArgs e);
        public delegate void ModelSelectionChangedEventHandler(object sender, EventArgs e);

        public CommitmentViewModel(Commitment model)
        {
            Model = model;
            RemoveCommitment = new RemoveCommitmentCommand();
            RemoveReminder = new RemoveReminderCommand();
            AddReminder = new AddReminderCommand();
        }

        public Commitment Model { protected set; get; }
        public ICommand RemoveCommitment { set; get; }
        public RemoveReminderCommand RemoveReminder { get; set; }
        public AddReminderCommand AddReminder { get; set; }
        public event ModelDeletedEventHandler Deleted;
        public event ModelSelectionChangedEventHandler SelectionChanged;

        public string LineOne
        {
            get
            {
                return Model.Content;
            }
        }

        public void RefreshRemoveReminder()
        {
            RemoveReminder.RaiseCanExecutionChanged();
        }

        public void RefreshAddReminder()
        {
            AddReminder.RaiseCanExecutionChanged();
        }

        private string lineTwo;
        public string LineTwo
        {
            get
            {
                if (lineTwo == null)
                {
                    lineTwo = GenerateLineTwo();
                }
                return lineTwo;
            }
            set
            {
                if (lineTwo != value)
                {
                    NotifyPropertyChanging("LineTwo");
                    lineTwo = value;
                    NotifyPropertyChanged("LineTwo");
                }
            }
        }

        private string lineThree;
        public string LineThree
        {
            get
            {
                if (lineThree == null)
                {
                    lineThree = GenerateLineThree();
                }
                return lineThree;
            }
            set
            {
                if (lineThree != value)
                {
                    NotifyPropertyChanging("LineThree");
                    lineThree = value;
                    NotifyPropertyChanged("LineThree");
                }
            }
        }

        private string GenerateLineThree()
        {
            var lastdone = this.Model.GetLastDone();
            int timesInARow = this.Model.GetConsecutiveDone(lastdone);
            return GetMotivationalText(timesInARow);
        }

        internal bool Internal { get; set; }

        private string GenerateLineTwo()
        {
            const string format = "{0}, {1} time{2} in a row";

            var lastdone = this.Model.GetLastDone();
            var lastdoneStr = Commitment.GetLastDoneString(lastdone);
            int timesInARow = this.Model.GetConsecutiveDone(lastdone);
            var times = timesInARow > 1 ? "s" : "";

            return string.Format(format, lastdoneStr, timesInARow, times);
        }

        private string GetMotivationalText(int timesInARow)
        {
            if (timesInARow == 0) return "you'll get it next time";
            if (timesInARow == 1) return "it wasn't that hard, was it?";
            if (timesInARow < 4) return "keep going...";
            if (timesInARow == 5) return "you are doing great!";
            if (timesInARow < 10) return "little more and this won't be necessary";
            if (timesInARow < 14) return "is this your habit already?";
            if (timesInARow < 21) return "it should be your habit already";
            if (timesInARow == 21) return "congratulations! You did it like a pro.";
            if (timesInARow < 25) return "you can probably delete this commitment now";
            if (timesInARow < 30) return "really...";
            if (timesInARow < 33) return "seriously.";
            if (timesInARow < 35) return "delete it, now!";
            if (timesInARow < 36) return "wondering what will be here next?";
            if (timesInARow < 37) return "me too!";
            if (timesInARow < 38) return "you know this cannot go on forever, right?";
            if (timesInARow < 40) return "cheers for Football Manager team!";
            if (timesInARow < 42) return "now texts will be changed less frequently";
            if (timesInARow < 45) return "seriously, delete this commitment";
            if (timesInARow < 50) return "you are really tough guy";
            if (timesInARow < 60) return "but I'm tougher";
            if (timesInARow < 70) return "you know it basically copy-paste for me?";
            if (timesInARow < 80) return "and I can just stop now?";
            if (timesInARow < 90) return "ok. ok. One more and you have to pay me";
            if (timesInARow < 100) return "one hundred is near";
            if (timesInARow == 100) return "one hundred. respect";
            if (timesInARow < 150) return "how long will you endure?";
            if (timesInARow < 200) return "you don't want to change phone to newer model?";
            if (timesInARow == 365) return "amazing, you did it! one year!";
            if (timesInARow == 366) return "good bye";
            return "";
        }

        private bool? hasNotification = null;
        public bool HasNotification
        {
            get
            {
                if (hasNotification == null)
                {
                    hasNotification = ServiceLocator.Reminders.GetReminderFor(this.Model) != null;
                }
                return hasNotification ?? false;
            }
            set
            {
                if (hasNotification != value)
                {
                    NotifyPropertyChanging("HasNotification");
                    hasNotification = value;
                    if (hasNotification ?? false)
                        ServiceLocator.Reminders.CreateNew(DateTime.Now, this.Model);
                    else
                    {
                        var not = ServiceLocator.Reminders.GetReminderFor(this.Model);
                        if (not != null)
                        {
                            ServiceLocator.Reminders.RemoveNotification(not);
                        }
                    }
                    NotifyPropertyChanged("HasNotification");
                }
            }
        }

        public DateTime? Reminder
        {
            get
            {
                return Model.Reminder;
            }
            set
            {
                NotifyPropertyChanging("Reminder");
                Model.Reminder = value;
                NotifyPropertyChanged("Reminder");
            }
        }

        public void Delete()
        {
            if (this.HasNotification)
            {
                this.HasNotification = false;
            }
            lock (ServiceLocator.DbContext)
                this.Model.Delete(ServiceLocator.DbContext);
            if (Deleted != null)
                Deleted(this, EventArgs.Empty);
        }

        private bool shareMode;
        public bool ShareMode
        {
            get
            {
                return shareMode;
            }
            set
            {
                if (shareMode != value)
                {
                    NotifyPropertyChanging("ShareMode");
                    shareMode = value;
                    NotifyPropertyChanged("IsSelectionCheckboxVisible");
                    NotifyPropertyChanged("IsSelectionCheckboxNotVisible");
                    NotifyPropertyChanged("ShareMode");
                }
            }
        }

        private bool selected;
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (selected != value)
                {
                    NotifyPropertyChanging("Selected");
                    selected = value;
                    NotifyPropertyChanged("Selected");
                    if (SelectionChanged != null)
                        SelectionChanged(this, EventArgs.Empty);
                }
            }
        }

        public Visibility IsSelectionCheckboxVisible
        {
            get
            {
                return ShareMode ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility IsSelectionCheckboxNotVisible
        {
            get
            {
                return ShareMode ? Visibility.Collapsed : Visibility.Visible;
            }
        }
    }
}