﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class RemoveCommitmentCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged { add {} remove {} }

        public void Execute(object parameter)
        {
            CommitmentViewModel rvm = (CommitmentViewModel)parameter;
            if (rvm.Model == null)
                throw new ArgumentNullException();

            if (rvm.Internal || Confirm())
            {
                rvm.Delete();
                lock (ServiceLocator.DbContext)
                {
                    ServiceLocator.CallendarManager.RefreshCurrentMonth();
                    if (!ServiceLocator.DbContext.Commitments.Any())
                        DbSeeders.AddStartupResolution(ServiceLocator.DbContext, false);
                }
            }
            MyLiveTile.UpdateLiveTile();
        }

        protected virtual bool Confirm()
        {
            MessageBoxResult m = MessageBox.Show(
                "Are you sure? This will delete your commitment. This operation cannot be undone.",
                "Confirmation", MessageBoxButton.OKCancel);
            return m == MessageBoxResult.OK;
        }
    }
}
