﻿using BartqHabitCallendar.Model;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using BartqHabitCallendar.Code;
using System.Threading.Tasks;

namespace BartqHabitCallendar.ViewModels
{
    public class TodayCommitmentViewModel : CommitmentViewModel
    {
        private DateTime date;
        public TodayCommitmentViewModel(Commitment model, DateTime date)
            : base(model)
        {
            this.date = date;
            MarkDoneCommand = new MarkCommitmentDone();
        }

        public TodayCommitmentViewModel(Commitment model)
            : this(model, DateTime.Today)
        {
        }

        private Visibility progressMarkingDone = Visibility.Collapsed;
        public Visibility ProgressMarkingDone
        {
            get
            {
                return progressMarkingDone;
            }
            set
            {
                if (progressMarkingDone != value)
                {
                    NotifyPropertyChanging("ProgressMarkingDone");
                    progressMarkingDone = value;
                    NotifyPropertyChanged("ProgressMarkingDone");
                }
            }
        }

        public ICommand MarkDoneCommand { get; set; }

        protected bool? done = null;

        public bool Done
        {
            get
            {
                if (!done.HasValue)
                {
                    done = Model.CommitmentDates.Any(rd => rd.Date.Value == date);
                }
                return done ?? false;
            }
            set
            {
                if (done != value)
                {
                    NotifyPropertyChanging("Done");
                    ProgressMarkingDone = Visibility.Visible;
                    done = value;
                    Task.Run(() =>
                    {
                        if (done.HasValue)
                            if (done.Value)
                            {
                                lock (ServiceLocator.DbContext)
                                    Model.DoneOn(ServiceLocator.DbContext, date);
                                if (HasNotification)
                                    Postpone();
                            }
                            else
                            {
                                try
                                {
                                    lock (ServiceLocator.DbContext)
                                        Model.UnDoneOn(ServiceLocator.DbContext, date);
                                    if (HasNotification)
                                        BringBack();
                                }
                                catch (InvalidOperationException ex)
                                {
                                    done = true;
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            NotifyPropertyChanged("Done");
                            NotifyPropertyChanged("ImageUri");
                            ProgressMarkingDone = Visibility.Collapsed;
                            LineTwo = null;
                            LineThree = null;
                            Recalculate();
                            MyLiveTile.UpdateLiveTile();
                            App.ViewModel.RefreshAllItems(Model.Id);
                        });
                    });
                }
            }
        }

        private void BringBack()
        {
            var res = ServiceLocator.Reminders.GetReminderFor(Model);
            DateTime nextperiod = res.BeginTime;
            int hour = Model.Reminder.Value.Hour;
            int minute = Model.Reminder.Value.Minute;
            int second = Model.Reminder.Value.Second;
            TimeSpan time = new TimeSpan(hour, minute, second);
            switch (Model.Type)
            {
                case Commitment.RType.EveryDay:
                    nextperiod = DateTime.Today;
                    break;
                case Commitment.RType.OnceAWeek:
                    nextperiod = DateTime.Today.AddWeek(1).AddDays(-1);
                    break;
                case Commitment.RType.OnceAMonth:
                    nextperiod = DateTime.Today.AddDays(-DateTime.Today.Day + 1).AddMonths(1);
                    break;
            }
            res.BeginTime = nextperiod.Add(time);
            ServiceLocator.Reminders.Reschedule(res);
        }

        private void Postpone()
        {
            var res = ServiceLocator.Reminders.GetReminderFor(Model);
            DateTime nextperiod = res.BeginTime;
            TimeSpan time = Model.Reminder.Value.TimeOfDay;
            switch (Model.Type)
            {
                case Commitment.RType.EveryDay:
                    nextperiod = DateTime.Today.AddDays(1);
                    break;
                case Commitment.RType.OnceAWeek:
                    nextperiod = DateTime.Today.AddWeek(2).AddDays(-1);
                    break;
                case Commitment.RType.OnceAMonth:
                    nextperiod = DateTime.Today.AddDays(-DateTime.Today.Day + 1).AddMonths(2);
                    break;
            }
            res.BeginTime = nextperiod.Add(time);
            ServiceLocator.Reminders.Reschedule(res);
        }

        private void Recalculate()
        {
            switch (this.Model.Type)
            {
                case Commitment.RType.EveryDay:
                    ServiceLocator.CallendarManager.RecalculateDay(date);
                    break;
                case Commitment.RType.OnceAWeek:
                    ServiceLocator.CallendarManager.RecalculateWeek(DateTime.Now);
                    break;
                case Commitment.RType.OnceAMonth:
                    ServiceLocator.CallendarManager.RecalculateMonth(DateTime.Now);
                    break;
            }
        }

        public string ImageUri
        {
            get
            {
                if (Done)
                    return "/Assets/appbar.clipboard.paper.check.png";
                else return "/Assets/appbar.clipboard.paper.png";
            }
        }
    }
}