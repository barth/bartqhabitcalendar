﻿using BartqHabitCallendar.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class RemoveReminderCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            CommitmentViewModel rvm = (CommitmentViewModel)parameter;
            return rvm != null && rvm.HasNotification;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            CommitmentViewModel rvm = (CommitmentViewModel)parameter;
            if (rvm.Model == null)
                throw new ArgumentNullException();

            if (Confirm())
                lock (ServiceLocator.DbContext)
                {
                    rvm.HasNotification = false;
                    rvm.Model.ReminderUuid = null;
                    ServiceLocator.DbContext.SubmitChanges();
                    RaiseCanExecutionChanged();
                    rvm.RefreshAddReminder();
                }
        }

        protected virtual bool Confirm()
        {
            MessageBoxResult m = MessageBox.Show(
                "Are you sure? This will delete reminder for your commitment.",
                "Confirmation", MessageBoxButton.OKCancel);
            return m == MessageBoxResult.OK;
        }

        public void RaiseCanExecutionChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
