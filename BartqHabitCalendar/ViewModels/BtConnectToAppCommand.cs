﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Networking.Proximity;

namespace BartqHabitCallendar.ViewModels
{
    public class BtConnectToAppCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            MainViewModel.PeerAppInfo appInfo = (MainViewModel.PeerAppInfo)parameter;
            appInfo.ExecuteExternal(appInfo);
        }
    }
}
