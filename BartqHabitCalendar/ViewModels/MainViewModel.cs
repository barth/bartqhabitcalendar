﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using BartqHabitCallendar.Resources;
using BartqHabitCallendar.Test;
using BartqHabitCallendar.Model;
using System.Collections.Generic;
using BartqHabitCallendar.Code;
using Windows.Networking.Proximity;
using System.Windows.Input;
using System.Windows;

namespace BartqHabitCallendar.ViewModels
{
    public class MainViewModel : PropertyChangingBase
    {
        public MainViewModel()
        {
            this.TodayItems = new ObservableCollection<TodayCommitmentViewModel>();
            this.AllItems = new ObservableCollection<CommitmentViewModel>();
            this.PeerApps = new ObservableCollection<PeerAppInfo>();
            this.ChartItems = new ObservableCollection<ChartPoint>();
            this.SelectedToDo = this.todoListItems.First();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<TodayCommitmentViewModel> TodayItems { get; private set; }
        public ObservableCollection<CommitmentViewModel> AllItems { get; private set; }
        private ObservableCollection<ChartPoint> chartItems;
        public ObservableCollection<ChartPoint> ChartItems
        {
            get
            {
                return chartItems;
            }
            set
            {
                chartItems = value;
                NotifyPropertyChanged("ChartItems");
            }
        }
        private DateTime selectedCalendarDate;
        public DateTime SelectedCalendarDate
        {
            get
            {
                return selectedCalendarDate;
            }
            set
            {
                if (value != selectedCalendarDate)
                {
                    NotifyPropertyChanging("SelectedCalendarDate");
                    selectedCalendarDate = value;
                    NotifyPropertyChanged("SelectedCalendarDate");
                    RefreshDayDetails();
                }
            }
        }

        private void RefreshDayDetails()
        {
            dayDetails = CalendarItemManager.CalculateDay(selectedCalendarDate, null, null, true).OrderByDescending(di => di.Right).ToList();
            NotifyPropertyChanged("DayDetails");
        }

        private int? todayDone = null;
        public int TodayDone
        {
            get
            {
                if (todayDone == null)
                {
                    BartqHabitCallendar.Code.CalendarItemManager.DailySuccessInfo dsi =
                        new BartqHabitCallendar.Code.CalendarItemManager.DailySuccessInfo(DateTime.Today);
                    CalendarItemManager.CalculateDay(DateTime.Today, null, dsi, false);
                    todayDone = (int)(dsi.Percent * 100.0);
                }
                return todayDone ?? 0;
            }
            set
            {
                if (todayDone != value)
                {
                    NotifyPropertyChanging("TodayDone");
                    todayDone = value;
                    NotifyPropertyChanged("TodayDone");
                }
            }
        }

        public enum ProgressModeEnum { Calendar, Chart }

        private ProgressModeEnum progressMode = ProgressModeEnum.Calendar;
        public ProgressModeEnum ProgressMode
        {
            get
            {
                return progressMode;
            }

            set
            {
                if (progressMode != value)
                {
                    progressMode = value;
                    NotifyPropertyChanged("CalendarVisibility");
                    NotifyPropertyChanged("ChartVisibility");
                }
            }
        }

        public Visibility CalendarVisibility
        {
            get
            {
                return ProgressMode == ProgressModeEnum.Calendar ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ChartVisibility
        {
            get
            {
                return ProgressMode == ProgressModeEnum.Calendar ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private List<BartqHabitCallendar.Code.CalendarItemManager.Desc> dayDetails;
        public List<BartqHabitCallendar.Code.CalendarItemManager.Desc> DayDetails
        {
            get
            {
                return dayDetails;
            }
        }

        public bool IsDataLoaded
        {
            get;
            internal set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData(DbContext dc)
        {

            this.AllItems.Clear();
            this.TodayItems.Clear();
            var todayitems = new List<TodayCommitmentViewModel>();
            foreach (var r in dc.Commitments)
            {
                if (ForDate(r, CurrentDate))
                {
                    var trvm = new TodayCommitmentViewModel(r, CurrentDate);
                    trvm.Deleted += item_Deleted;
                    trvm.SelectionChanged += today_SelectionChanged;
                    todayitems.Add(trvm);
                }

                var rvm = new CommitmentViewModel(r);
                rvm.Deleted += item_Deleted;
                rvm.SelectionChanged += all_SelectionChanged;
                this.AllItems.Add(rvm);
            }

            var todayordered = todayitems.OrderBy(td => td.Done).ThenBy(td => td.Model.Type);
            foreach (var item in todayordered)
                this.TodayItems.Add(item);

            GenerateChartData();

            this.IsDataLoaded = true;
        }

        private void GenerateChartData()
        {
            ObservableCollection<ChartPoint> data = new ObservableCollection<ChartPoint>();

            var today = DateTime.Today;
            var currDate = today.AddDays(-30);
            var source = ServiceLocator.CallendarManager.cache;
            
            while (currDate <= today)
            {
                BartqHabitCallendar.Code.CalendarItemManager.DailySuccessInfo dsi;

                if (!source.ContainsKey(currDate))
                    ServiceLocator.CallendarManager.RecalculateMonth(currDate);

                if (source.TryGetValue(currDate, out dsi))
                {
                    data.Add(new ChartPoint(currDate, dsi.Percent * 100));
                }

                currDate = currDate.AddDays(1);
            }

            this.ChartItems = data;
        }

        public Action<bool> OnAnySelected { get; set; }
        public Action<bool> OnTodaySelected { get; set; }

        public bool AnySelected
        {
            get
            {
                return AllItems.Any(i => i.Selected);
            }
        }
        public bool TodaySelected
        {
            get
            {
                return TodayItems.Any(i => i.Selected);
            }
        }

        void all_SelectionChanged(object sender, EventArgs e)
        {
            if (OnAnySelected != null)
                OnAnySelected(AnySelected);
        }

        void today_SelectionChanged(object sender, EventArgs e)
        {
            if (OnTodaySelected != null)
                OnTodaySelected(TodaySelected);
        }

        internal void item_Deleted(object sender, EventArgs e)
        {
            var res = (CommitmentViewModel)sender;
            TodayCommitmentViewModel td = this.TodayItems.FirstOrDefault(x => x.Model.Id == res.Model.Id);
            if (td != null)
                this.TodayItems.Remove(td);
            CommitmentViewModel rd = this.AllItems.FirstOrDefault(x => x.Model.Id == res.Model.Id);
            if (rd != null)
                this.AllItems.Remove(rd);
        }

        public void LoadData()
        {
            lock (ServiceLocator.DbContext)
                LoadData(ServiceLocator.DbContext);
        }

        public static bool ForToday(Commitment r)
        {
            return ForDate(r, DateTime.Today);
        }

        public static bool ForDate(Commitment r, DateTime date)
        {
            if (r.Type == Commitment.RType.EveryDay) return true;
            DateTime periodStart = DateTime.MinValue;
            if (r.Type == Commitment.RType.OnceAWeek)
            {
                periodStart = date.AddWeek(0.0);
            }
            if (r.Type == Commitment.RType.OnceAMonth)
            {
                periodStart = DateTime.Today.AddDays(-date.Day + 1);
            }
            var donethisperiod = r.CommitmentDates.Where(rd => rd.Date.Value >= periodStart)
                .OrderByDescending(rd => rd.Date.Value)
                .FirstOrDefault();

            return donethisperiod == null || donethisperiod.Date.Value == date;
        }

        public void AppendNewTodayCommitment(Commitment newCommitment)
        {

            var todayres = new TodayCommitmentViewModel(newCommitment, CurrentDate);
            todayres.Deleted += item_Deleted;
            todayres.SelectionChanged += today_SelectionChanged;
            this.TodayItems.Insert(0, todayres);
            this.AllItems.Insert(0, new CommitmentViewModel(newCommitment));
        }

        void todayres_SelectionChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private bool shareMode;
        public bool ShareMode
        {
            get
            {
                return shareMode;
            }
            set
            {
                if (value != shareMode)
                {
                    NotifyPropertyChanging("ShareMode");
                    shareMode = value;
                    SetShareMode(this.AllItems, value);
                    SetShareMode(this.TodayItems, value);
                    NotifyPropertyChanged("ShareMode");
                }
            }
        }

        private void SetShareMode<T>(ObservableCollection<T> observableCollection, bool value) where T : CommitmentViewModel
        {
            foreach (var item in observableCollection)
            {
                item.ShareMode = value;
            }
        }

        public ObservableCollection<PeerAppInfo> PeerApps { get; private set; }

        /// <summary>
        ///  Class to hold all peer information
        /// </summary>
        public class PeerAppInfo
        {
            internal static BtConnectToAppCommand ConnectCommand = new BtConnectToAppCommand();

            internal PeerAppInfo(PeerInformation peerInformation, Action<PeerAppInfo> ex)
            {
                this.PeerInfo = peerInformation;
                this.DisplayName = this.PeerInfo.DisplayName;
                this.ExecuteExternal = ex;
            }

            public string DisplayName { get; private set; }
            public PeerInformation PeerInfo { get; private set; }

            public Action<PeerAppInfo> ExecuteExternal { get; private set; }
            public ICommand ConnectBluetoothCommand
            {
                get
                {
                    return ConnectCommand;
                }
            }
        }

        internal void RefreshAllItems(int p)
        {
            var vm = this.AllItems.Where(ai => ai.Model.Id == p).FirstOrDefault();
            if (vm != null)
            {
                vm.LineTwo = null;
                vm.LineThree = null;
            }
            RefreshDayDetails();
            GenerateChartData();
        }

        public class TodoListItem : PropertyChangingBase
        {
            public string Label { get; set; }
            private Visibility selected = Visibility.Collapsed;
            public Visibility Selected
            {
                get
                {
                    return selected;
                }
                set
                {
                    if (selected != value)
                    {
                        NotifyPropertyChanging("Selected");
                        selected = value;
                        NotifyPropertyChanged("Selected");
                    }
                }
            }
        }

        private List<TodoListItem> todoListItems = new List<TodoListItem>() { 
            new TodoListItem() { Label = "today", Selected=Visibility.Visible }, 
            new TodoListItem() { Label = "yesterday" } 
        };

        public List<TodoListItem> TodoListItems
        {
            get
            {
                return todoListItems;
            }
        }

        private TodoListItem selectedToDo;
        public TodoListItem SelectedToDo
        {
            get
            {
                return selectedToDo;
            }
            set
            {
                if (selectedToDo != value)
                {
                    NotifyPropertyChanging("SelectedToDo");
                    if (selectedToDo != null)
                        selectedToDo.Selected = Visibility.Collapsed;
                    value.Selected = Visibility.Visible;
                    selectedToDo = value;
                    LoadData();
                    NotifyPropertyChanged("SelectedToDo");
                }
            }
        }

        public DateTime CurrentDate
        {
            get
            {
                DateTime date = DateTime.Today;
                if (SelectedToDo.Label == "yesterday")
                {
                    date = date.AddDays(-1);
                }
                return date;
            }
        }
    }
}