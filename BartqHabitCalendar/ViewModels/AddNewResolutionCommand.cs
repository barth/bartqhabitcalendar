﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using BartqHabitCallendar.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class AddNewCommitmentCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            AddNewViewModel rvm = (AddNewViewModel)parameter;
            return rvm != null && !string.IsNullOrWhiteSpace(rvm.CommitmentText);
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            AddNewViewModel model = (AddNewViewModel)parameter;
            lock (ServiceLocator.DbContext)
            {
                JustAdded = Commitment.CreateNew(ServiceLocator.DbContext, model.CommitmentText, model.SelectedType.ItemType);
                DbSeeders.RemoveStartupResolution(ServiceLocator.DbContext);//sooner there will be moment with

                //no commitments and code to add default commitment will be triggered
                if (model.ReminderEnabled)
                {
                    JustAdded.Reminder = model.Reminder;
                    var res = ServiceLocator.Reminders.CreateNew(model.Reminder, JustAdded);
                    JustAdded.ReminderUuid = res.Name;//connecting remider to commitment
                    ServiceLocator.DbContext.SubmitChanges();
                }
            }
            if (!model.Internal)
                App.ViewModel.AppendNewTodayCommitment(JustAdded);
            ServiceLocator.NavigationService.GoBack();
            ServiceLocator.CallendarManager.RefreshCurrentMonth();
            MyLiveTile.UpdateLiveTile();
        }

        public Commitment JustAdded { get; private set; }

        public void RaiseCanExecutionChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
