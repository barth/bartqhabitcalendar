﻿using BartqHabitCallendar.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class MarkCommitmentDone : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged { add { } remove { } }

        public void Execute(object parameter)
        {
            TodayCommitmentViewModel rvm = (TodayCommitmentViewModel)parameter;
            if (rvm.Model == null)
                throw new ArgumentNullException();
            rvm.Done = !rvm.Done;
        }
    }
}
