﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class AddNewViewModel : PropertyChangingBase
    {
        public class TypeItem
        {
            public Commitment.RType ItemType { get; set; }
            public string ItemText
            {
                get
                {
                    return ItemType.GetAttributeOfType<DescriptionAttribute>().Description;
                }
            }
        }

        private string commitmentText;

        public string CommitmentText
        {
            get
            {
                return commitmentText;
            }
            set
            {
                if (value != commitmentText)
                {
                    NotifyPropertyChanging("CommitmentText");
                    commitmentText = value;
                    NotifyPropertyChanged("CommitmentText");
                    AddNew.RaiseCanExecutionChanged();
                }
            }
        }

        public AddNewCommitmentCommand AddNew { get; private set; }

        public List<TypeItem> TypeItems { get; private set; }

        private TypeItem selectedType;
        public TypeItem SelectedType
        {
            get
            {
                return selectedType;
            }
            set
            {
                if (value != SelectedType)
                {
                    NotifyPropertyChanging("SelectedType");
                    selectedType = value;
                    NotifyPropertyChanged("SelectedType");
                }
            }
        }

        private DateTime reminder;
        public DateTime Reminder
        {
            get
            {
                return reminder;
            }
            set
            {
                if (reminder != value)
                {
                    NotifyPropertyChanging("Reminder");
                    reminder = value;
                    NotifyPropertyChanged("Reminder");
                }
            }
        }

        private bool reminderEnabled;
        public bool ReminderEnabled
        {
            get
            {
                return reminderEnabled;
            }
            set
            {
                if (reminderEnabled != value)
                {
                    NotifyPropertyChanging("ReminderEnabled");
                    reminderEnabled = value;
                    NotifyPropertyChanged("ReminderEnabled");
                    NotifyPropertyChanged("ReminderVisible");
                }
            }
        }

        public AddNewViewModel()
        {
            TypeItems = new List<TypeItem>();
            foreach (Commitment.RType type in Enum.GetValues(typeof(Commitment.RType)))
            {
                TypeItems.Add(new TypeItem() { ItemType = type });
            }
            SelectedType = TypeItems.FirstOrDefault();
            ReminderEnabled = true;
            Reminder = DateTime.Today.AddHours(18);
            AddNew = new AddNewCommitmentCommand();
        }

        internal bool Internal { get; set; }
    }
}
