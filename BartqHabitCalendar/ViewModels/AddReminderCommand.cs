﻿using BartqHabitCallendar.Code;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BartqHabitCallendar.ViewModels
{
    public class AddReminderCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            CommitmentViewModel rvm = (CommitmentViewModel)parameter;
            return rvm != null && !rvm.HasNotification;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            CommitmentViewModel rvm = (CommitmentViewModel)parameter;
            if (rvm.Model == null)
                throw new ArgumentNullException();

            MakeDialog(rvm);
        }

        private void MakeDialog(CommitmentViewModel rvm)
        {
            TimePicker timePicker = new TimePicker()
            {
                Value = DateTime.Today.AddHours(18)
            };

            string str = "day";
            if (rvm.Model.Type == Model.Commitment.RType.OnceAWeek)
                str = "week";
            else if (rvm.Model.Type == Model.Commitment.RType.OnceAMonth)
                str = "month";

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Title = "Bartq Habit Caledar",
                Caption = "Add reminder",
                Message = "Remind me that i will " + rvm.Model.Content + " every " + str + " at",
                Content = timePicker,
                LeftButtonContent = "add",
                RightButtonContent = "cancel",
                IsFullScreen = false
            };

            messageBox.Dismissing += (s1, e1) =>
            {
                //if (timePicker. == ListPickerMode.Expanded)
                //{
                //    e1.Cancel = true;
                //}
            };

            messageBox.Dismissed += (s2, e2) =>
            {
                switch (e2.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        AddReminderFor(rvm, timePicker.Value);
                        RaiseCanExecutionChanged();
                        rvm.RefreshRemoveReminder();
                        break;
                    case CustomMessageBoxResult.RightButton:
                    case CustomMessageBoxResult.None:
                        // just ignore
                        break;
                    default:
                        break;
                }
            };

            messageBox.Show();
        }

        private void AddReminderFor(CommitmentViewModel rvm, DateTime? time)
        {
            lock (ServiceLocator.DbContext)
                if (time.HasValue)
                {
                    rvm.Model.Reminder = time;
                    var res = ServiceLocator.Reminders.CreateNew(time.Value, rvm.Model);
                    rvm.Model.ReminderUuid = res.Name;//connecting remider to commitment
                    rvm.HasNotification = true;
                    ServiceLocator.DbContext.SubmitChanges();
                }
        }

        public void RaiseCanExecutionChanged()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
