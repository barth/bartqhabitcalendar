﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BartqHabitCallendar.ViewModels;
using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using Microsoft.Phone.Scheduler;

namespace BartqHabitCallendar
{
    public partial class AddNew : PhoneApplicationPage
    {
        public AddNew()
        {
            InitializeComponent();

            DataContext = new AddNewViewModel();
            TypeListPicker.SizeChanged += TypeListPicker_SizeChanged;
        }

        void TypeListPicker_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var picker = sender as ListPicker;
            if (picker != null)
            {
                picker.EnsureVisibleInScrollViewer();
            }
            
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}