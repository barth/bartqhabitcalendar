﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Model
{
    [Table]
    public class Setting : PropertyChangingBase, INotifyPropertyChanged, INotifyPropertyChanging
    {
        protected string key;
        [Column(IsPrimaryKey = true, IsDbGenerated = false, CanBeNull = false, AutoSync = AutoSync.Never)]
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                if (key != value)
                {
                    NotifyPropertyChanging("key");
                    key = value;
                    NotifyPropertyChanged("key");
                }
            }
        }

        protected string val;
        [Column]
        public string Value
        {
            get
            {
                return val;
            }
            set
            {
                if (val != value)
                {
                    NotifyPropertyChanging("val");
                    val = value;
                    NotifyPropertyChanged("val");
                }
            }
        }

        public static void Insert(DbContext dc,string key, string val)
        {
            Setting s = new Setting() { Key = key, Value = val };
            dc.Settings.InsertOnSubmit(s);
            dc.SubmitChanges();
        }

        public static void InsertOrUpdate(DbContext dc, string key, string val)
        {
            var s = Read(dc, key);
            if (s == null)
                Insert(dc, key, val);
            else
            {
                s.Value = val;
                dc.SubmitChanges();
            }
        }

        public static Setting Read(DbContext dc, string key)
        {
            return dc.Settings.FirstOrDefault(s => s.Key == key);
        }

        internal void Delete(DbContext dc)
        {
            dc.Settings.DeleteOnSubmit(this);
            dc.SubmitChanges();
        }
    }
}
