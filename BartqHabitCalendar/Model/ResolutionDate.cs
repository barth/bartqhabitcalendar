﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Model
{
    [Table]
    public class CommitmentDate : PropertyChangingBase
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = false, DbType = "INT NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public int CommitmentId { get; set; }

        [Column(IsPrimaryKey = true, IsDbGenerated = false, DbType = "INT NOT NULL", CanBeNull = false, AutoSync = AutoSync.Never)]
        public int DateId { get; set; }

        protected EntityRef<Commitment> commitment;
        [Association(ThisKey = "CommitmentId", OtherKey = "Id", Storage = "commitment", IsForeignKey=true)]
        public Commitment Commitment
        {
            get
            {
                return commitment.Entity;
            }
            set
            {
                commitment.Entity = value;
            }
        }

        protected EntityRef<Date> date;
        [Association(ThisKey = "DateId", OtherKey = "Id", Storage = "date", IsForeignKey=true)]
        public Date Date
        {
            get
            {
                return date.Entity;
            }
            set
            {
                date.Entity = value;
            }
        }
    }
}
