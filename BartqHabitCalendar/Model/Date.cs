﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace BartqHabitCallendar.Model
{
    [Table]
    [Index(Columns = "Value", IsUnique=true)]
    public class Date : PropertyChangingBase, INotifyPropertyChanged, INotifyPropertyChanging
    {
        protected int id;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    NotifyPropertyChanging("id");
                    id = value;
                    NotifyPropertyChanged("id");
                }
            }
        }

        protected DateTime date;

        [Column]
        public DateTime Value
        {
            get
            {
                return date;
            }
            set
            {
                if (date != value)
                {
                    NotifyPropertyChanging("date");
                    date = value;
                    NotifyPropertyChanged("date");
                }
            }
        }

        [Association(ThisKey = "Id", OtherKey = "DateId")]
        public EntitySet<CommitmentDate> CommitmentDates { get; set; }

        public Date()
        {
            CommitmentDates = new EntitySet<CommitmentDate>();
        }
    }
}
