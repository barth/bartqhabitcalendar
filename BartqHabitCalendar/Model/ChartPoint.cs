﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Model
{
    public class ChartPoint
    {
        //Create a model
        public DateTime X { get; set; }
        public double Y { get; set; }

        public ChartPoint(DateTime x, double y)
        {
            X = x;
            Y = y;
        }

    }
}
