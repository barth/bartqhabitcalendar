﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace BartqHabitCallendar.Model
{
    public class DbContext : DataContext
    {
        // Specify the connection string as a static, used in main page and app.xaml.
        protected static string DBConnectionString = "Data Source=isostore:/bhcdb.sdf";//;Password='outh98wr32bklfsd9'";

        // Pass the connection string to the base class.
        protected DbContext(bool @internal)
            : base(DBConnectionString)
        {
            
        }

        public DbContext()
            : this(true)
        {
            if (!this.DatabaseExists())
            {
                this.CreateDatabase();
            }
        }

        public Table<Commitment> Commitments;
        public Table<Date> Dates;
        public Table<CommitmentDate> CommitmentDates;
        public Table<Setting> Settings;
    }
}
