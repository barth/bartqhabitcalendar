﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Microsoft.Phone.Data.Linq.Mapping;
using BartqHabitCallendar.Code;
using System.Windows;

namespace BartqHabitCallendar.Model
{
    [Table]
    [Index(Columns = "StartDate ASC")]
    [Index(Columns = "_type")]
    public class Commitment : PropertyChangingBase, INotifyPropertyChanged, INotifyPropertyChanging
    {
        public enum RType { 
            [Description("day")]
            EveryDay=0, 
            [Description("week")]
            OnceAWeek, 
            [Description("month")]
            OnceAMonth 
        }

        // Define ID: private field, public property and database column.
        protected int id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    NotifyPropertyChanging("id");
                    id = value;
                    NotifyPropertyChanged("id");
                }
            }
        }

        protected string content;

        [Column]
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                if (content != value)
                {
                    NotifyPropertyChanging("Content");
                    content = value;
                    NotifyPropertyChanged("Content");
                }
            }
        }

        protected DateTime? reminder;

        [Column]
        public DateTime? Reminder
        {
            get
            {
                return reminder;
            }
            set
            {
                if (reminder != value)
                {
                    NotifyPropertyChanging("Reminder");
                    reminder = value;
                    NotifyPropertyChanged("Reminder");
                }
            }
        }

        protected string reminderUuid;

        [Column]
        public string ReminderUuid
        {
            get
            {
                return reminderUuid;
            }
            set
            {
                if (reminderUuid != value)
                {
                    NotifyPropertyChanging("ReminderUuid");
                    reminderUuid = value;
                    NotifyPropertyChanged("ReminderUuid");
                }
            }
        }

        protected int type;

        [Column]
        public int _type
        {
            get
            {
                return type;
            }
            set
            {
                if (type != value)
                {
                    NotifyPropertyChanging("_type");
                    type = value;
                    NotifyPropertyChanged("_type");
                }
            }
        }

        public RType Type
        {
            get
            {
                return (RType)type;
            }
            set
            {
                int val = (int)value;
                if (type != val)
                {
                    NotifyPropertyChanging("Type");
                    type = val;
                    NotifyPropertyChanged("Type");
                }
            }
        }

        protected DateTime startDate;
        [Column(CanBeNull = false)]
        
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (startDate != value)
                {
                    NotifyPropertyChanging("StartDate");
                    startDate = value;
                    NotifyPropertyChanged("StartDate");
                }
            }
        }


        [Association(ThisKey = "Id", OtherKey = "CommitmentId")]
        public EntitySet<CommitmentDate> CommitmentDates { get; set; }

        public Commitment()
        {
            CommitmentDates = new EntitySet<CommitmentDate>();
            StartDate = DateTime.Today;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {1}", this.id, this.Type, this.Content);
        }

        public void DoneOn(DbContext dc, DateTime date)
        {
            var dt = InsertDateIfNotPresent(dc, date);

            var exists = dc.CommitmentDates.Where(rd => rd.DateId == dt.Id && rd.CommitmentId == this.Id).FirstOrDefault();

            if (exists == null)
            {
                CommitmentDate rd = new CommitmentDate()
                {
                    DateId = dt.Id,
                    CommitmentId = this.Id
                };

                dc.CommitmentDates.InsertOnSubmit(rd);
                this.CommitmentDates.Add(rd);
                dt.CommitmentDates.Add(rd);

                dc.SubmitChanges();
            }
        }

        public static Date InsertDateIfNotPresent(DbContext dc, DateTime date)
        {
            var dt = dc.Dates.FirstOrDefault(d => d.Value == date);
            if (dt == null)
            {
                dt = new Date()
                {
                    Value = date
                };
                dc.Dates.InsertOnSubmit(dt);
                dc.SubmitChanges();
            }
            return dt;
        }

        public static Commitment CreateNew(DbContext dc, string content, RType type)
        {
            Commitment r = new Commitment()
            {
                Content = content,
                Type = type,
                Reminder = null
            };
            dc.Commitments.InsertOnSubmit(r);
            dc.SubmitChanges();
            return r;
        }

        public void UnDoneOn(DbContext dc, DateTime date)
        {
            var rd = this.CommitmentDates.FirstOrDefault(rrd => rrd.Date.Value == date);
            if (rd != null)
            {
                var toremove = dc.CommitmentDates
                    .Single(cd => cd.CommitmentId == rd.CommitmentId && cd.DateId == rd.DateId);
                dc.CommitmentDates.DeleteOnSubmit(toremove);
                this.CommitmentDates.Remove(rd);
                rd.Date.CommitmentDates.Remove(toremove);
                dc.SubmitChanges();
            }
            else
            {
                throw new InvalidOperationException("You cannot undone this commitment because it is too late for this.");
            }
        }

        public void Delete(DbContext dc)
        {
            dc.CommitmentDates.DeleteAllOnSubmit(this.CommitmentDates);
            var rds = this.CommitmentDates.ToList();
            foreach(var rd in rds)
            {
                this.CommitmentDates.Remove(rd);
            }
            dc.Commitments.DeleteOnSubmit(this);
            dc.SubmitChanges();
        }


        public DateTime? GetLastDone()
        {
            var ld = CommitmentDates
                .OrderByDescending(rd => rd.Date.Value)
                .FirstOrDefault();
            if (ld != null)
                return ld.Date.Value;
            return null;
        }

        public static string GetLastDoneString(DateTime? lastdone)
        {
            if(lastdone==null)
                return "never done";
            var ago = DateTime.Today - lastdone.Value.Date;
            if (ago.TotalDays < 1)
                return "date";
            if (ago.TotalDays < 2)
                return "yesterday";
            if (ago.TotalDays < 8)
                return string.Format("{0:d} days ago", (int)ago.TotalDays);
            return lastdone.Value.Date.ToShortDateString();
        }

        public int GetConsecutiveDone(DateTime? lastdone)
        {
            int timesInARow = 0;
            var currdate = lastdone;
            while (currdate != null)
            {
                timesInARow++;
                DateTime prevdate = currdate ?? DateTime.Today;
                switch (this.Type)
                {
                    case Commitment.RType.EveryDay:
                        prevdate = currdate.Value.AddDays(-1);
                        break;
                    case Commitment.RType.OnceAWeek:
                        prevdate = currdate.Value.AddWeek(-1.0);
                        break;
                    case Commitment.RType.OnceAMonth:
                        prevdate = currdate.Value.AddDays(-currdate.Value.Day + 1).AddMonths(-1);
                        break;
                }
                var ld = this.CommitmentDates
                    .Where(rd => rd.Date.Value >= prevdate && rd.Date.Value < currdate)
                    .FirstOrDefault();
                if (ld != null)
                    currdate = ld.Date.Value;
                else currdate = null;
            }
            return timesInARow;
        }
    }
}
