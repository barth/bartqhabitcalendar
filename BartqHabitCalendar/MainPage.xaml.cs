﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using BartqHabitCallendar.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using Windows.ApplicationModel.Activation;
using Windows.Networking.Proximity;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using Windows.Storage.Streams;

namespace BartqHabitCallendar
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Error code constants
        const uint ERR_BLUETOOTH_OFF = 0x8007048F;      // The Bluetooth radio is off
        const uint ERR_MISSING_CAPS = 0x80070005;       // A capability is missing from your WMAppManifest.xml
        const uint ERR_NOT_ADVERTISING = 0x8000000E;    // You are currently not advertising your presence using PeerFinder.Start()

        enum Page { Today, Progress, List };

        Page CurrentPage;

        public const string PROXIMITY_MESSAGE_TYPE = "Windows.BartqHCComShare";
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            App.ViewModel.OnAnySelected = OnAnySelectedHandler;
            App.ViewModel.OnTodaySelected = OnTodaySelectedHandler;

            ServiceLocator.CallendarManager.SetCalendar(this.Calendar);
            BackKeyPress += MainPage_BackKeyPress;
            ApplicationBar.StateChanged += ApplicationBar_StateChanged;
            UpdateAppbar();

            //AdMediator_814481.AdSdkError += AdMediator_Bottom_AdError;
            //AdMediator_814481.AdMediatorFilled += AdMediator_Bottom_AdFilled;
            //AdMediator_814481.AdMediatorError += AdMediator_Bottom_AdMediatorError;
            //AdMediator_814481.AdSdkEvent += AdMediator_Bottom_AdSdkEvent;
            RecreateButton("share");
            NfcRegisterForMessage();

            AddTodayDateIfNotPresent();

            // Register for incoming connection requests
            PeerFinder.ConnectionRequested += PeerFinder_ConnectionRequested;
            //PeerFinder.DisplayName = "Bartq Habit Calendar";
            PeerFinder.Start();
        }

        private void AddTodayDateIfNotPresent()
        {
            Task.Run(() =>
            {
                lock (ServiceLocator.DbContext)
                    Commitment.InsertDateIfNotPresent(ServiceLocator.DbContext, DateTime.Today);
            });
        }

        void OnAnySelectedHandler(bool selected)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            if (mv.ShareMode && CurrentPage == Page.List)
            {
                AppBarButton_IsEnabled("nfc", mv.AnySelected);
                AppBarButton_IsEnabled("bluetooth", mv.AnySelected);
            }
        }

        void OnTodaySelectedHandler(bool selected)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            if (mv.ShareMode && CurrentPage == Page.Today)
            {
                AppBarButton_IsEnabled("nfc", mv.TodaySelected);
                AppBarButton_IsEnabled("bluetooth", mv.TodaySelected);
            }
        }

        void MainPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = CancelSharing();
        }

        private bool CancelSharing()
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            if (mv.ShareMode)
            {
                mv.ShareMode = false;
                AppBarButton_IsVisible("share", true, 0);
                AppBarButton_IsVisible("add new", true, 0);
                AppBarButton_IsVisible("nfc", false);
                AppBarButton_IsVisible("bluetooth", false);

                if (NfcPopup.IsOpen)
                {
                    StopPublishingMessage();
                    NfcPopup.IsOpen = false;
                }
                if (BluetoothPopup.IsOpen)
                {
                    BluetoothPopup.IsOpen = false;
                }

                return true;
            }
            return false;
        }

        void AdMediator_Bottom_AdSdkEvent(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
        {
            Debug.WriteLine("AdSdk event {0} by {1}", e.EventName, e.Name);
        }

        void AdMediator_Bottom_AdMediatorError(object sender, Microsoft.AdMediator.Core.Events.AdMediatorFailedEventArgs e)
        {
            Debug.WriteLine("AdMediatorError:" + e.Error + " " + e.ErrorCode);
            // if (e.ErrorCode == AdMediatorErrorCode.NoAdAvailable)
            // AdMediator will not show an ad for this mediation cycle
        }

        void AdMediator_Bottom_AdFilled(object sender, Microsoft.AdMediator.Core.Events.AdSdkEventArgs e)
        {
            Debug.WriteLine("AdFilled:" + e.Name);
        }

        void AdMediator_Bottom_AdError(object sender, Microsoft.AdMediator.Core.Events.AdFailedEventArgs e)
        {
            Debug.WriteLine("AdSdkError by {0} ErrorCode: {1} ErrorDescription: {2} Error: {3}", e.Name, e.ErrorCode, e.ErrorDescription, e.Error);
        }

        public void UpdateAppbar()
        {
            foreach (ApplicationBarMenuItem menuitem in ApplicationBar.MenuItems)
            {
                if (menuitem.Text.Contains("tile"))
                {
                    menuitem.IsEnabled = !MyLiveTile.TileCreated();
                }
            }
        }

        void ApplicationBar_StateChanged(object sender, ApplicationBarStateChangedEventArgs e)
        {
            if (e.IsMenuVisible)
                UpdateAppbar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (ServiceLocator.FileSavePickerContinuationArgs != null)
            {
                object type;
                if (ServiceLocator.FileSavePickerContinuationArgs.ContinuationData.TryGetValue("Operation", out type))
                {
                    string strtype = type as string;
                    if (!string.IsNullOrWhiteSpace(strtype))
                    {
                        if (strtype == CExportLabel)
                        {
                            this.ContinueFileSavePicker(ServiceLocator.FileSavePickerContinuationArgs);
                            ServiceLocator.FileSavePickerContinuationArgs = null;
                        }
                    }
                }

            }
            if (ServiceLocator.FileLoadPickerContinuationArgs != null)
            {
                object type;
                if (ServiceLocator.FileLoadPickerContinuationArgs.ContinuationData.TryGetValue("Operation", out type))
                {
                    string strtype = type as string;
                    if (!string.IsNullOrWhiteSpace(strtype))
                    {
                        if (strtype == CImportLabel)
                        {
                            this.ContinueFileOpenPicker(ServiceLocator.FileLoadPickerContinuationArgs);
                            ServiceLocator.FileLoadPickerContinuationArgs = null;
                        }
                    }
                }

            }
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs e)
        {
            StorageFile file = e.Files.SingleOrDefault();
            if (file != null)
            {
                // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
                // write to file
                var buffer = await FileIO.ReadBufferAsync(file);
                var bdata = System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions.ToArray(buffer);
                var content = FullTransferPackage.Decrypt(bdata);
                var data = FullTransferPackage.Deserialize(content);
                // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
                // Completing updates may require Windows to ask for user input.
                if (MessageBox.Show("Are you sure to REPLACE current application state with data from this file? " +
                    "There will be no undo option, but you can export it first. Proceed?", "Import", MessageBoxButton.OKCancel)
                    == MessageBoxResult.OK)
                {
                    lock (ServiceLocator.DbContext)
                        data.ImportTo(ServiceLocator.DbContext);
                }
                else
                {
                    // File was not saved
                }
            }
            else
            {
                // File Operation cancelled
            }
        }

        private async void ContinueFileSavePicker(FileSavePickerContinuationEventArgs e)
        {
            StorageFile file = e.File;
            if (file != null)
            {
                // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
                CachedFileManager.DeferUpdates(file);
                FullTransferPackage data;
                lock (ServiceLocator.DbContext)
                    data = FullTransferPackage.Create(ServiceLocator.DbContext);
                var content = data.Serialize();
                var bdata = FullTransferPackage.Encrypt(content);
                var buffer = System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions.AsBuffer(bdata);
                // write to file
                await FileIO.WriteBufferAsync(file, buffer);
                // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
                // Completing updates may require Windows to ask for user input.
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == FileUpdateStatus.Complete)
                {
                    MessageBox.Show("Your data was saved.", "Success", MessageBoxButton.OK);
                }
                else
                {
                    // File was not saved
                }
            }
            else
            {
                // File Operation cancelled
            }
        }

        private void AddNewClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddNew.xaml", UriKind.Relative));
        }

        private void ShareClick(object sender, EventArgs e)
        {
            StartSharing();
        }

        private void StartSharing()
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            mv.ShareMode = true;
            AppBarButton_IsVisible("share", false);
            AppBarButton_IsVisible("add new", false);
            AppBarButton_IsVisible("nfc", true);
            AppBarButton_IsEnabled("nfc", CurrentPage == Page.Today ? mv.TodaySelected : mv.AnySelected);
            AppBarButton_IsVisible("bluetooth", true);
            AppBarButton_IsEnabled("bluetooth", CurrentPage == Page.Today ? mv.TodaySelected : mv.AnySelected);
        }

        long? nfcSubscriptionId = null;

        private void NfcRegisterForMessage()
        {
            var device = ServiceLocator.ProximityDevice;
            if (device != null)
            {
                nfcSubscriptionId = device.SubscribeForMessage(PROXIMITY_MESSAGE_TYPE, NfcMessageReceived);
            }
        }

        private void NfcUnRegisterForMessage()
        {
            if (nfcSubscriptionId.HasValue)
            {
                var device = ServiceLocator.ProximityDevice;
                if (device != null)
                {
                    device.StopSubscribingForMessage(nfcSubscriptionId.Value);
                    nfcSubscriptionId = null;
                }
            }
        }

        void NfcMessageReceived(ProximityDevice sender, ProximityMessage message)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                var data = message.DataAsString;

                try
                {
                    var package = BaseTransferPackage.Deserialize(data);
                    lock (ServiceLocator.DbContext)
                        package.ImportTo(ServiceLocator.DbContext);
                }
                catch (Exception e)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(e.Message, "Something went wrong :(", MessageBoxButton.OK);
                    });
                }
            });
        }

        private void Calendar_SelectionChanged(object sender, WPControls.SelectionChangedEventArgs e)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            mv.SelectedCalendarDate = e.SelectedDate;
        }

        private void ApplicationBarMenuItemAbout_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItemTile_Click(object sender, EventArgs e)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            List<TodayCommitmentViewModel> tiii;
            List<TodayCommitmentViewModel> tdiii;
            GetDataForLiveTail(mv, out tiii, out tdiii);
            MyLiveTile.CreateLiveTile(tiii, tdiii);
        }

        public static void GetDataForLiveTail(MainViewModel mv, out List<TodayCommitmentViewModel> todo, out List<TodayCommitmentViewModel> done)
        {
            todo = mv.TodayItems.Where(td => !td.Done).ToList();
            done = mv.TodayItems.Where(td => td.Done).ToList();
        }

        private void ApplicationBarMenuItemExport_Click(object sender, EventArgs e)
        {
            //TextBox tb = new TextBox();

            //CustomMessageBox messageBox = new CustomMessageBox()
            //{
            //    Caption = "Exporting data",
            //    Message =
            //        "Please enter your PIN code or leave it empty to use default one."
            //        ,
            //    Content = tb,
            //    LeftButtonContent = "ok",
            //    RightButtonContent = "cancel",
            //    IsFullScreen = false
            //};

            //messageBox.Dismissed += (s1, e1) =>
            //{
            //    switch (e1.Result)
            //    {
            //        case CustomMessageBoxResult.LeftButton:
            ExportFile(null);//tb.Text
            //            break;
            //        case CustomMessageBoxResult.RightButton:
            //        case CustomMessageBoxResult.None:
            //            break;
            //        default:
            //            break;
            //    }
            //};

            //messageBox.Show();  
        }

        private const string CExportLabel = "export";
        private const string CImportLabel = "import";

        private void ExportFile(string pwd)
        {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("BartqHabitCalendar encrypted data", new List<string>() { ".bhced" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "data";

            savePicker.ContinuationData["Operation"] = CExportLabel;
            savePicker.PickSaveFileAndContinue();
        }

        private void ApplicationBarMenuItemImport_Click(object sender, EventArgs e)
        {
            ImportFile("");
        }

        private void ImportFile(string pwd)
        {
            FileOpenPicker savePicker = new FileOpenPicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeFilter.Add(".bhced");
            savePicker.ContinuationData["Operation"] = CImportLabel;
            savePicker.PickSingleFileAndContinue();
        }

        private void Panorama_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Chart.MinHeight = ProgressGrid.ActualHeight;
            MainViewModel mv = (MainViewModel)this.DataContext;
            if (mv.ShareMode)
                CancelSharing();
            if (e.AddedItems.Count > 0)
            {

                var added = e.AddedItems[0] as PanoramaItem;
                if (added != null && added.Header != null)
                {
                    CurrentPage = added.Header.ToString() == "list" ? Page.List : Page.Progress;

                    AppBarButton_IsVisible("share", !mv.ShareMode && (CurrentPage == Page.Today || CurrentPage == Page.List));
                }
                else
                {
                    CurrentPage = Page.Today;
                    AppBarButton_IsVisible("share", !mv.ShareMode, 1);
                }

                AppBarButton_IsVisible("calendar", mv.ProgressMode == MainViewModel.ProgressModeEnum.Chart && CurrentPage == Page.Progress);
                AppBarButton_IsVisible("chart", mv.ProgressMode == MainViewModel.ProgressModeEnum.Calendar && CurrentPage == Page.Progress);
            }
        }

        private void AppBarButton_IsEnabled(string text, bool enabled)
        {
            for (int i = 0; i < ApplicationBar.Buttons.Count; i++)
            {
                var button = (ApplicationBarIconButton)ApplicationBar.Buttons[i];
                if (button.Text == text)
                {
                    button.IsEnabled = enabled;
                }
            }
        }

        private void AppBarButton_IsVisible(string text, bool p, int position = -1)
        {
            if (p)
            {
                for (int i = 0; i < ApplicationBar.Buttons.Count; i++)
                {
                    var button = (ApplicationBarIconButton)ApplicationBar.Buttons[i];
                    if (button.Text == text)
                    {
                        return;
                    }
                }
                RecreateButton(text, position);
            }
            else
            {
                for (int i = 0; i < ApplicationBar.Buttons.Count; i++)
                {
                    var button = (ApplicationBarIconButton)ApplicationBar.Buttons[i];
                    if (button.Text == text)
                    {
                        ApplicationBar.Buttons.RemoveAt(i);
                        break;
                    }
                }
            }

        }

        private void RecreateButton(string text, int position = -1)
        {
            ApplicationBarIconButton b = null;
            switch (text)
            {
                case "share":
                    b = new ApplicationBarIconButton();
                    b.Text = "share";
                    b.IconUri = new Uri("/Assets/appbar.social.sharethis.png", UriKind.Relative);
                    b.Click += ShareClick;

                    break;
                case "add new":
                    b = new ApplicationBarIconButton();
                    b.Text = text;
                    b.IconUri = new Uri("/Assets/appbar.add.png", UriKind.Relative);
                    b.Click += AddNewClick;
                    break;
                case "nfc":
                    b = new ApplicationBarIconButton();
                    b.Text = "nfc";
                    b.IconUri = new Uri("/Assets/appbar.nfc.png", UriKind.Relative);
                    b.Click += NfcClick;
                    break;
                case "bluetooth":
                    b = new ApplicationBarIconButton();
                    b.Text = "bluetooth";
                    b.IconUri = new Uri("/Assets/appbar.connection.bluetooth.png", UriKind.Relative);
                    b.Click += BluetoothClick;
                    break;
                case "calendar":
                    b = new ApplicationBarIconButton();
                    b.Text = "calendar";
                    b.IconUri = new Uri("/Assets/appbar.calendar.month.png", UriKind.Relative);
                    b.Click += ShowCalendar;
                    break;
                case "chart":
                    b = new ApplicationBarIconButton();
                    b.Text = "chart";
                    b.IconUri = new Uri("/Assets/appbar.graph.line.png", UriKind.Relative);
                    b.Click += ShowGraph;
                    break;
            }
            if (b != null)
            {
                if (position == -1)
                    ApplicationBar.Buttons.Add(b);
                else ApplicationBar.Buttons.Insert(position, b);
            }
        }

        public long? proximityMessageId = null;

        void NfcClick(object sender, EventArgs e)
        {

            ProximityDevice device = ServiceLocator.ProximityDevice;

            // Make sure NFC is supported
            if (device != null)
            {
                var msg = GenerateTransferMessage();
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    proximityMessageId = device.PublishMessage(PROXIMITY_MESSAGE_TYPE, msg, NfcMessageTransmitted);
                    Debug.WriteLine("Published Message. ID is {0}", proximityMessageId);
                    ShowNfcPopup();
                }
                // Store the unique message Id so that it 
                // can be used to stop publishing this message
            }
        }

        private void StartProgress(string message)
        {
            BluetoothProgress.Visibility = System.Windows.Visibility.Visible;
            BluetoothText.Text = message;
        }

        private void StopProgress()
        {
            BluetoothProgress.Visibility = System.Windows.Visibility.Collapsed;
        }

        async void BluetoothClick(object sender, EventArgs e)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;

            await RefreshBtPeers();
            ShowBtPopup();
        }

        void ShowCalendar(object sender, EventArgs e)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            mv.ProgressMode = MainViewModel.ProgressModeEnum.Calendar;
            AppBarButton_IsVisible("calendar", mv.ProgressMode == MainViewModel.ProgressModeEnum.Chart && CurrentPage == Page.Progress);
            AppBarButton_IsVisible("chart", mv.ProgressMode == MainViewModel.ProgressModeEnum.Calendar && CurrentPage == Page.Progress);
        }

        void ShowGraph(object sender, EventArgs e)
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            mv.ProgressMode = MainViewModel.ProgressModeEnum.Chart;
            AppBarButton_IsVisible("calendar", mv.ProgressMode == MainViewModel.ProgressModeEnum.Chart && CurrentPage == Page.Progress);
            AppBarButton_IsVisible("chart", mv.ProgressMode == MainViewModel.ProgressModeEnum.Calendar && CurrentPage == Page.Progress);
        }

        private async Task RefreshBtPeers()
        {
            MainViewModel mv = (MainViewModel)this.DataContext;
            try
            {
                StartProgress("finding peers ...");
                //PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
                var peers = await PeerFinder.FindAllPeersAsync();
                // By clearing the backing data, we are effectively clearing the ListBox
                mv.PeerApps.Clear();
                if (peers.Count == 0)
                {
                    BluetoothText.Text = "No peers found";
                }
                else
                {

                    // Add peers to list
                    foreach (var peer in peers)
                    {
                        mv.PeerApps.Add(new BartqHabitCallendar.ViewModels.MainViewModel.PeerAppInfo(peer, BluetoothConnectToApp));
                    }

                    //If there is only one peer, go ahead and select it
                    if (PeerList.Items.Count == 1)
                    {
                        BluetoothText.Text = "Found one active peer";
                        PeerList.SelectedIndex = 0;
                    }
                    else
                    {
                        BluetoothText.Text = "Found " + peers.Count + " active peers. Select one.";
                    }

                }
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == ERR_BLUETOOTH_OFF)
                {
                    var result = MessageBox.Show("Bluetooth is off. Press yes to show Bluetooth settings.", "Bluetooth is off", MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        ShowBluetoothControlPanel();
                    }
                }
                else
                {
                    MessageBox.Show(ex.Message);
                }
            }
            finally
            {
                StopProgress();
            }
        }

        private void PeerFinder_ConnectionRequested(object sender, ConnectionRequestedEventArgs args)
        {
            ReadFromPeer(args.PeerInformation);
        }

        private void ReadFromPeer(PeerInformation peerInformation)
        {
            this.Dispatcher.BeginInvoke(async () =>
                {
                    try
                    {
                        BaseTransferPackage package = null;
                        using (var socket = await PeerFinder.ConnectAsync(peerInformation))
                        {
                            using (var dataWriter = new DataWriter(socket.OutputStream))
                            {
                                dataWriter.WriteByte(0);
                                await dataWriter.StoreAsync();
                                await dataWriter.FlushAsync();
                                using (var dataReader = new DataReader(socket.InputStream))
                                {
                                    await dataReader.LoadAsync(4);
                                    var len = (uint)dataReader.ReadInt32();
                                    //first we send length of the message
                                    await dataReader.LoadAsync(len);
                                    var buf = dataReader.ReadBuffer(len);
                                    var bufarr = buf.ToArray();
                                    var msg = Encoding.UTF8.GetString(bufarr, 0, bufarr.Length);
                                    package = BaseTransferPackage.Deserialize(msg);
                                }
                                dataWriter.WriteByte(0);
                                await dataWriter.StoreAsync();
                                await dataWriter.FlushAsync();
                            }
                        }
                        if (package != null)
                        {
                            lock (ServiceLocator.DbContext)
                                package.ImportTo(ServiceLocator.DbContext);
                        }
                    }
                    catch (Exception ex)
                    {
                        // In this sample, we handle each exception by displaying it and
                        // closing any outstanding connection. An exception can occur here if, for example, 
                        // the connection was refused, the connection timeout etc.
                        MessageBox.Show(ex.Message + ". Bluetooth module may need a restart before another attempt.");
                    }
                    finally
                    {
                        CancelSharing();
                    }
                });
        }

        private void ShowBluetoothControlPanel()
        {
            ConnectionSettingsTask connectionSettingsTask = new ConnectionSettingsTask();
            connectionSettingsTask.ConnectionSettingsType = ConnectionSettingsType.Bluetooth;
            connectionSettingsTask.Show();
        }

        private void ShowNfcPopup()
        {
            double height = Application.Current.Host.Content.ActualHeight;
            double width = Application.Current.Host.Content.ActualWidth;
            NfcPopupContentPanel.Width = width;
            NfcPopupContentPanel.Height = height;
            AppBarButton_IsVisible("bluetooth", false);
            NfcPopup.IsOpen = true;
        }

        private void ShowBtPopup()
        {
            double height = Application.Current.Host.Content.ActualHeight;
            double width = Application.Current.Host.Content.ActualWidth;
            BluetoothPopupContentPanel.Width = width;
            BluetoothPopupContentPanel.Height = height;
            AppBarButton_IsVisible("nfc", false);
            AppBarButton_IsVisible("bluetooth", false);
            BluetoothPopup.IsOpen = true;
        }


        private string GenerateTransferMessage()
        {
            HashSet<int> selectedIDs = new HashSet<int>();
            MainViewModel mv = (MainViewModel)this.DataContext;
            if (CurrentPage == Page.Today)
            {
                foreach (var item in mv.TodayItems)
                    if (item.Selected)
                        selectedIDs.Add(item.Model.Id);
            }
            else
                if (CurrentPage == Page.List)
                {
                    foreach (var item in mv.AllItems)
                        if (item.Selected)
                            selectedIDs.Add(item.Model.Id);
                }
            BaseTransferPackage package = null;
            lock (ServiceLocator.DbContext)
                package = BaseTransferPackage.Create(ServiceLocator.DbContext, selectedIDs);
            return package.Serialize();
        }

        void NfcMessageTransmitted(ProximityDevice sender, long messageId)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    CancelSharing();
                });
        }

        private void StopPublishingMessage()
        {
            if (proximityMessageId.HasValue)
            {
                ProximityDevice device = ServiceLocator.ProximityDevice;

                // Make sure NFC is supported
                if (device != null)
                {
                    device.StopPublishingMessage(proximityMessageId.Value);
                    proximityMessageId = null;
                }
            }
        }

        private void BluetoothConnectToApp(MainViewModel.PeerAppInfo pdi)
        {
            PeerInformation peer = pdi.PeerInfo;
            StartProgress("Connecting");
            SendToPeer(peer);
            StopProgress();
            CancelSharing();
        }

        void SendToPeer(PeerInformation peer)
        {
            this.Dispatcher.BeginInvoke(async () =>
            {
                try
                {
                    using (var socket = await PeerFinder.ConnectAsync(peer))
                    {
                        using (var dataReader = new DataReader(socket.InputStream))
                        {
                            await dataReader.LoadAsync(1);//we wait for readiness for receiving
                            dataReader.ReadByte();
                            using (var dataWriter = new DataWriter(socket.OutputStream))
                            {
                                var msg = GenerateTransferMessage();
                                //first we send length of the message
                                dataWriter.WriteInt32(msg.Length);
                                await dataWriter.StoreAsync();
                                //then the message
                                dataWriter.WriteString(msg);
                                await dataWriter.StoreAsync();
                                //we assume that connection can be closed now (?)
                                await dataWriter.FlushAsync();
                                await dataReader.LoadAsync(1);//we wait now to the end of communication
                                dataReader.ReadByte();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // In this sample, we handle each exception by displaying it and
                    // closing any outstanding connection. An exception can occur here if, for example, 
                    // the connection was refused, the connection timeout etc.
                    MessageBox.Show(ex.Message + ". Sometimes restarting your bt module before another attempt helps.");
                }
            });
        }

        async private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            await RefreshBtPeers();
        }
    }
}