﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Test
{
    //based on http://damieng.com/blog/2008/07/30/linq-to-sql-log-to-debug-window-file-memory-or-multiple-writers
    public class DebugTextWriter : TextWriter
    {
        public override void Write(char[] buffer, int index, int count)
        {
            Debug.WriteLine(new String(buffer, index, count));
        }

        public override void Write(string value)
        {
            Debug.WriteLine(value);
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
