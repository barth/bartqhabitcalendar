﻿using BartqHabitCallendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Test
{
    public class SeededDbContext : DbContext
    {
        protected bool outputLog;
        public bool OutputLog
        {
            get
            {
                return outputLog;
            }
            set {
                if (outputLog != value)
                {
                    outputLog = value;
                    if (outputLog)
                        this.Log = new DebugTextWriter();
                    else
                        this.Log = null;
                }
            }
        }
        
        public SeededDbContext(Action<DbContext> seed, bool recreate)
            : base(true)
        {
            if (this.DatabaseExists())
            {
                if (recreate)
                {
                    this.DeleteDatabase();
                    this.CreateDatabase();
                    seed(this);
                }
            }
            else
            {
                this.CreateDatabase();
                seed(this);
            }
#if DEBUG
            OutputLog = true;
#endif
        }
    }
}
