﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using BartqHabitCallendar.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Test
{
    public class DbSeeders
    {
        public static void Seed(DbContext dc)
        {
            Setting.Insert(dc,"DBVersion",ServiceLocator.VersionProvider.DBVersion);
            Setting.Insert(dc,"AppVersion",ServiceLocator.VersionProvider.Version);
            Setting.Insert(dc,"InstallDate",DateTime.Now.ToShortDateString());
            AddStartupResolution(dc,true);
        }

        public static void TestSeed(DbContext dc)
        {
            #region everyday
            var today = DateTime.Today;
            Commitment evdonetoday = Commitment.CreateNew(dc,"#1 everyday done date", Commitment.RType.EveryDay);
            evdonetoday.DoneOn(dc,today);

            var yesterday = today.AddDays(-1);
            Commitment evnotdonetoday = Commitment.CreateNew(dc,"#2 everyday not done date", Commitment.RType.EveryDay);
            evnotdonetoday.DoneOn(dc,yesterday);

            Commitment evneverdone = Commitment.CreateNew(dc,"#3 everyday never done", Commitment.RType.EveryDay);
            #endregion

            #region weekly
            var mondaythisweek = today;
            while (mondaythisweek.DayOfWeek != DayOfWeek.Monday)
                mondaythisweek = mondaythisweek.AddDays(-1);
            var lastweek = mondaythisweek.AddDays(-2);

            Commitment evweeklynotdonethisweek = Commitment.CreateNew(dc,"#4 weekly not done this week", Commitment.RType.OnceAWeek);
            evweeklynotdonethisweek.DoneOn(dc, lastweek);

            Commitment evweeklydonethisweek = Commitment.CreateNew(dc,"#5 weekly done this week", Commitment.RType.OnceAWeek);
            evweeklydonethisweek.DoneOn(dc,mondaythisweek);


            Commitment weeklydonetoday = Commitment.CreateNew(dc,"#6 weekly done date", Commitment.RType.OnceAWeek);
            weeklydonetoday.DoneOn(dc,today);
            #endregion

            #region monthly
            var lastmonth = today.AddMonths(-1);
            Commitment monthlynotdonethismonth = Commitment.CreateNew(dc, "#7 monthly not done this month", Commitment.RType.OnceAMonth);
            monthlynotdonethismonth.DoneOn(dc,lastmonth);

            var thismonth = today.AddDays(-today.Day + 1);
            Commitment monthlydonethismonth = Commitment.CreateNew(dc, "#8 monthly done this month", Commitment.RType.OnceAMonth);
            monthlydonethismonth.DoneOn(dc, thismonth);

            Commitment monthlydonetoday = Commitment.CreateNew(dc, "#9 monthly done this date", Commitment.RType.OnceAMonth);
            monthlydonethismonth.DoneOn(dc, today);
            #endregion
        }

        public const string StartupResolutionText = "add some commitments";

        public static void AddStartupResolution(DbContext dc, bool @internal)
        {
            var vm = AddNewCommitment(StartupResolutionText, @internal: @internal);
            Setting.Insert(dc, "StartupCommitment", vm.AddNew.JustAdded.Id.ToString());
        }

        public static void RemoveStartupResolution(DbContext dc)
        {
            var s = Setting.Read(dc, "StartupCommitment");
            if (s != null)
            {
                var id = int.Parse(s.Value);
                var sc = dc.Commitments.FirstOrDefault(c => c.Id == id);
                if (sc != null)
                {
                    CommitmentViewModel cvm = new CommitmentViewModel(sc);
                    cvm.Internal = true;//do not confirm
                    cvm.Deleted += App.ViewModel.item_Deleted;
                    cvm.RemoveCommitment.Execute(cvm);
                }
                s.Delete(dc);
            }
        }

        public static AddNewViewModel AddNewCommitment(string text,
            Commitment.RType type = Commitment.RType.EveryDay, bool @internal = false)
        {
            var model = new AddNewViewModel();
            //when
            model.CommitmentText = text;
            model.ReminderEnabled = true;
            model.Reminder = DateTime.Today.AddHours(20);
            model.SelectedType = model.TypeItems.First(t => t.ItemType == Commitment.RType.EveryDay);
            model.Internal = @internal; //dont add new vm item
            model.AddNew.Execute(model);
            ServiceLocator.Reminders.Refresh();
            return model;
        }
    }
}
