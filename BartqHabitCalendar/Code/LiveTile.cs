﻿using BartqHabitCallendar.ViewModels;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using Windows.Storage.Streams;

namespace BartqHabitCallendar.Code
{
    public class MyLiveTile
    {
        const string MediumTileFrontFile = "Shared/ShellContent/TileMediumFront.png";
        const string MediumTileBackFile = "Shared/ShellContent/TileMediumBack.png";
        const string LargeTileFrontFile = "Shared/ShellContent/TileLargeFront.png";
        const string LargeTileBackFile = "Shared/ShellContent/TileLargeBack.png";

        public static IEnumerable<ShellTile> GetTiles()
        {
            return ShellTile.ActiveTiles;
        }

        public static void UpdateLiveTile()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {

                List<TodayCommitmentViewModel> data;
                List<TodayCommitmentViewModel> done;

                MainPage.GetDataForLiveTail(App.ViewModel, out data, out done);

                var tiles = GetTiles();
                if (tiles != null)
                    foreach (var tile in tiles)
                    {
                        FlipTileData flipTile = CreateFlipTileData(data, done);

                        //Update Live Tile
                        tile.Update(flipTile);
                    }
                else
                {
                    // once it is created flip tile
                    //CreateLiveTile(filename);
                }
            });
        }

        public static void CreateLiveTile(List<TodayCommitmentViewModel> data, List<TodayCommitmentViewModel> todo)
        {
            try
            {
                Uri tileUri = new Uri("/MainPage.xaml?tile=flip", UriKind.Relative);
                ShellTileData tileData = CreateFlipTileData(data, todo);
                ShellTile.Create(tileUri, tileData, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Tworzenie kafelka nie powiodło się." + Environment.NewLine + e.Message);
            }
        }

        private static void CreateMediumTile(List<TodayCommitmentViewModel> data, int done)
        {
            //must be created on ui thread :[
            var img = GetBitmapImage("Assets/Tiles/FlipCycleTileMedium.png");
            var img2 = GetBitmapImage("Assets/Tiles/FlipCycleTileMedium.png");
            var wb = new WriteableBitmap(img);
            var c = CreateCanvas(wb, setBkColor: true);
            AddIcon(wb, c, img2);
            RenderText(c, 10, 20, "Bartq Habit Calendar", 30);
            var dne = done.ToString() + "%";
            while (dne.Length < 4)
                dne = " " + dne;
            RenderText(c, 190, 147, dne, 50, TextAlignment.Right);
            var cnt = data.Count().ToString();
            if (cnt.Length < 2)
                cnt = " " + cnt;
            if (cnt.Length > 2)
                cnt = "!?";
            RenderText(c, 68, 147, cnt, 50, TextAlignment.Center);
            RenderText(c, 10, 298, "date progress", 30);
            RenderAndSave(wb, c, MediumTileFrontFile);

            img = GetBitmapImage("Assets/Tiles/FlipCycleTileEmptyMedium.png");
            wb = new WriteableBitmap(img);
            c = CreateCanvas(wb, setBkColor: true);
            RenderText(c, 10, 20, "Bartq Habit Calendar", 30);
            var i = 75;
            foreach (var d in data.Take(4))
            {
                RenderText(c, 10, i, "• " + d.LineOne, 20, w: 315, h: 50);
                i += 50;
            }
            RenderText(c, 10, 298, "to do date", 30);
            RenderAndSave(wb, c, MediumTileBackFile);
        }

        private static BitmapImage GetBitmapImage(string s)
        {
            StreamResourceInfo resourceInfo;
            BitmapImage img;
            var uri = new Uri(s, UriKind.Relative);
            resourceInfo = Application.GetResourceStream(uri);
            img = new BitmapImage();
            img.SetSource(resourceInfo.Stream);
            return img;
        }

        private static void AddIcon(WriteableBitmap wb, Canvas c, BitmapImage img)
        {
            //c.Background = new SolidColorBrush(Colors.Transparent);
            var icon = new Image();
            icon.Margin = new Thickness(0);
            icon.Source = img;
            icon.Stretch = Stretch.Fill;
            icon.Height = img.PixelHeight;
            icon.Width = img.PixelWidth;
            Canvas.SetTop(icon, 0d);
            Canvas.SetLeft(icon, 0d);
            c.Children.Add(icon);
            //wb.Render(c, null);
            //wb.Invalidate(); //Draw bitmap
        }

        private static void CreateLargeTile(List<TodayCommitmentViewModel> data, List<TodayCommitmentViewModel> done)
        {
            var img = GetBitmapImage("Assets/Tiles/FlipCycleTileLarge.png");
            var img2 = GetBitmapImage("Assets/Tiles/FlipCycleTileLarge.png");
            var wb = new WriteableBitmap(img);
            var c = CreateCanvas(wb, setBkColor: true);
            AddIcon(wb, c, img2);
            RenderText(c, 10, 20, "Bartq Habit Calendar", 30);

            var cnt = data.Count().ToString();
            if (cnt.Length < 2)
                cnt = " " + cnt;
            if (cnt.Length > 2)
                cnt = "!?";
            RenderText(c, 64, 147, cnt, 50, TextAlignment.Center);
            var i = 65;
            foreach (var d in data.Take(4))
            {
                RenderText(c, 175, i, "• "+d.LineOne, 25, w: 480, h: 50);//+ d.LineOne
                i += 60;
            }
            RenderText(c, 10, 298, "to do date", 30);
            RenderAndSave(wb, c, LargeTileFrontFile);

            img = GetBitmapImage("Assets/Tiles/FlipCycleTileEmptyLarge.png");
            wb = new WriteableBitmap(img);
            c = CreateCanvas(wb, setBkColor: true);
            AddIcon(wb, c, img);
            RenderText(c, 10, 20, "Bartq Habit Calendar", 30);
            i = 65;
            foreach (var d in done.Take(7))
            {
                RenderText(c, 10, i, "• "+d.LineOne, 25, w: 1000, h: 50);//+ d.LineOne
                i += 30;
            }
            RenderText(c, 10, 298, "done date", 30);
            RenderAndSave(wb, c, LargeTileBackFile);
        }

        private static FlipTileData CreateFlipTileData(List<TodayCommitmentViewModel> data, List<TodayCommitmentViewModel> done)
        {
            FlipTileData flipTile = new FlipTileData();
            flipTile.Title = "";
            flipTile.BackTitle = "";

            var donecount = (int)(1.0 * done.Count / (data.Count + done.Count) * 100.0);

            CreateMediumTile(data, donecount);
            CreateLargeTile(data, done);

            flipTile.BackgroundImage = new Uri("isostore:/" + MediumTileFrontFile,
                UriKind.Absolute); //Default image for Background Image Medium Tile 336x336 px
            //End Medium size Tile 336x336 px
            flipTile.BackBackgroundImage = new Uri("isostore:/" + MediumTileBackFile,
                        UriKind.Absolute);

            flipTile.WideBackgroundImage = new Uri("isostore:/" + LargeTileFrontFile,
                UriKind.Absolute);

            flipTile.WideBackBackgroundImage = new Uri("isostore:/" + LargeTileBackFile,
                        UriKind.Absolute);

            //End Wide size Tile 691x336 px
            return flipTile;
        }

        private static Canvas CreateCanvas(WriteableBitmap b, bool setBkColor)
        {
            var canvas = new Canvas();
            canvas.Width = b.PixelWidth;
            canvas.Height = b.PixelHeight;
            if (setBkColor)
            {
                SolidColorBrush backColor = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]);
                canvas.Background = backColor;
            }
            return canvas;
        }

        private static void RenderText(Canvas canvas, double left, double top, string text, int fontsize, TextAlignment talign = TextAlignment.Left, int w = 0, int h = 0)
        {
            var textBlock = new TextBlock();

            textBlock.Text = text;
            textBlock.FontWeight = FontWeights.Bold;
            textBlock.TextAlignment = talign;
            if (talign == TextAlignment.Left)
                textBlock.HorizontalAlignment = HorizontalAlignment.Left;
            else if (talign == TextAlignment.Center)
                textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            else if (talign == TextAlignment.Right)
                textBlock.HorizontalAlignment = HorizontalAlignment.Right;
            textBlock.VerticalAlignment = VerticalAlignment.Top;
            textBlock.Margin = new Thickness(5);
            textBlock.LineStackingStrategy = LineStackingStrategy.BlockLineHeight;
            textBlock.TextWrapping = TextWrapping.Wrap;
            textBlock.Foreground = new SolidColorBrush(Colors.White); //color of the text on the Tile
            textBlock.FontSize = fontsize;
            Canvas.SetLeft(textBlock, left);
            Canvas.SetTop(textBlock, top);

            if (w != 0)
                textBlock.Width = w;
            if (h != 0)
                textBlock.Height = h;

            canvas.Children.Add(textBlock);
        }

        private static void RenderAndSave(WriteableBitmap b, Canvas canvas, string filename)
        {
            b.Render(canvas, null);
            b.Invalidate(); //Draw bitmap
            string dir = Path.GetDirectoryName(filename);
            //Save bitmap as jpeg file in Isolated Storage
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (!isf.DirectoryExists(dir))
                    isf.CreateDirectory(dir);
                if (isf.FileExists(filename))
                {
                    isf.DeleteFile(filename);
                }
                using (IsolatedStorageFileStream imageStream = new IsolatedStorageFileStream(filename, System.IO.FileMode.Create, isf))
                {
                    b.SaveJpeg(imageStream, b.PixelWidth, b.PixelHeight, 0, 100);
                }
            }
        }

        internal static bool TileCreated()
        {
            return MyLiveTile.GetTiles().Any(a => a.NavigationUri.ToString().Contains("/Pages/MainPage.xaml"));
        }
    }
}
