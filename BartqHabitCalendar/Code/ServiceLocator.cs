﻿using BartqHabitCallendar.Model;
using BartqHabitCallendar.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.ApplicationModel.Activation;
using Windows.Networking.Proximity;

namespace BartqHabitCallendar.Code
{
    public static class ServiceLocator
    {
        private static DbContext dbcontext;
        public static DbContext DbContext
        {
            get
            {
                if (dbcontext == null)
                    CreateDbContextIfNotExists();
                return dbcontext;
            }
        }

        public static void SetDbContext(DbContext dc)
        {
            dbcontext = dc;
        }

        private static INavigationService navigationService;
        public static INavigationService NavigationService
        {
            get
            {
                if (navigationService == null)
                    navigationService = new NavigationService();
                return navigationService;
            }
        }
        
        public static FileSavePickerContinuationEventArgs FileSavePickerContinuationArgs { get; set; }
        public static FileOpenPickerContinuationEventArgs FileLoadPickerContinuationArgs { get; set; }

        private static VersionProvider versionProvider;
        public static VersionProvider VersionProvider
        {
            get
            {
                if (versionProvider == null)
                    versionProvider = new VersionProvider();
                return versionProvider;
            }
        }

        private static ReminderHelper reminders;
        public static ReminderHelper Reminders
        {
            get
            {
                if (reminders == null)
                    reminders = new ReminderHelper();
                return reminders;
            }
        }

        private static ProximityDevice proximityDevice;
        public static ProximityDevice ProximityDevice
        {
            get
            {
                if (proximityDevice == null)
                {
                    proximityDevice = ProximityDevice.GetDefault();
                }
                return proximityDevice;
            }
        }

        public static void SetReminders(ReminderHelper rem) {
            reminders = rem;
        }

        public static void SetVersionProvider(VersionProvider vp)
        {
            versionProvider = vp;
        }

        public static void SetNavigationService(INavigationService service)
        {
            navigationService = service;
        }

        private static CalendarItemManager callendarManager;
        public static CalendarItemManager CallendarManager
        {
            get
            {
                var cim = Application.Current.Resources["ColorConverter"] as CalendarItemManager;
                if (callendarManager == null)
                {
                    if (cim != null)
                        callendarManager = cim;
                    else
                        callendarManager = new CalendarItemManager();
                }
                return callendarManager;
            }
        }

        private static void CreateDbContextIfNotExists()
        {
            if (ServiceLocator.dbcontext == null)
            {
                //#if DEBUG no recreation of db since i already have data
                //                DbContext = new SeededDbContext(DbSeeders.TestSeed, true);
                //#else
                ServiceLocator.dbcontext = new SeededDbContext(DbSeeders.Seed, false);
                //#endif
            }
        }

        public static void Dispose()
        {
            if (ServiceLocator.dbcontext != null)
            {
                ServiceLocator.dbcontext.Dispose();
                ServiceLocator.dbcontext = null;
            }
        }
    }
}
