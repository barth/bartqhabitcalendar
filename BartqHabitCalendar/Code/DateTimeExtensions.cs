﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Code
{
    public static class DateTimeExtensions
    {
        public static DateTime AddWeek(this DateTime dt, double value)
        {
            int dayOfWeek = (((int)dt.DayOfWeek) + 6) % 7;
            var ndt = dt.AddDays(-dayOfWeek);
            ndt = ndt.AddDays(value * 7.0);
            return ndt;
        }
    }
}
