﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Code
{
    public class VersionProvider
    {
        public virtual string DBVersion
        {
            get
            {
                return "1.0.0";
            }
        }

        public virtual string Version
        {
            get
            {
                string ver = "1.3.0";
#if DEBUG
                ver += " work";
#else
                
#endif
                return ver;
            }
        }
    }
}
