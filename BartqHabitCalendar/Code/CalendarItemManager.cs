﻿using BartqHabitCallendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using WPControls;

namespace BartqHabitCallendar.Code
{
    /// from http://wpcontrols.codeplex.com/
    /// and also https://code.msdn.microsoft.com/windowsapps/Customizing-windows-phone-d496724e
    public class CalendarItemManager : IDateToBrushConverter
    {
        private Calendar calendar;

        public void SetCalendar(Calendar cal)
        {
            this.calendar = cal;
            calendar.CustomFill = OnCalendarCustomFill;
            calendar.MonthChanging += calendar_MonthChanging;
            RecalculateMonth(DateTime.Today);
        }

        void calendar_MonthChanging(object sender, MonthChangedEventArgs e)
        {
            DateTime m = new DateTime(e.Year, e.Month, 1);
            RecalculateMonth(m);
        }

        public class DailySuccessInfo
        {
            public string Label { get; set; }
            public double Percent { get; set; }
            public DateTime Date { get; private set; }

            public DailySuccessInfo(DateTime date)
            {
                Date = date;
            }
        }

        public Dictionary<DateTime, DailySuccessInfo> cache = new Dictionary<DateTime, DailySuccessInfo>();

        public void OnCalendarCustomFill(CalendarItem item)
        {
            if (item is CalendarLabeledItem)
            {
                var li = (CalendarLabeledItem)item;
                var dsi = GetCache(li.ItemDate.Date);
                li.Label = dsi.Label;
            }
        }

        public DailySuccessInfo GetCache(DateTime item)
        {
            DailySuccessInfo dsi = null;
            if (!cache.ContainsKey(item))
            {
                dsi = new DailySuccessInfo(item);
                cache[item] = dsi;
            }
            else dsi = cache[item];
            return dsi;
        }

        public void RefreshCurrentMonth()
        {
            if (calendar != null)
                RecalculateMonth(calendar.SelectedDate);
        }

        public void Calculate(DateTime fromdate, DateTime todate)
        {
            var currdate = fromdate.Date;
            List<Commitment> commitments = null;
            lock (ServiceLocator.DbContext)
                commitments = ServiceLocator.DbContext.Commitments.Where(r => r.StartDate <= todate).ToList();
            if (commitments != null)
            {
                while (currdate <= todate)
                {
                    var dsi = GetCache(currdate);

                    CalculateDay(currdate, commitments, dsi);
                    currdate = currdate.AddDays(1);
                }
                if (calendar != null)
                    calendar.Refresh();
            }
        }

        public class Desc
        {
            public string Left { get; set; }
            public string Right { get; set; }
        }

        public static List<Desc> CalculateDay(DateTime currdate, List<Model.Commitment> commitments, DailySuccessInfo dsi, bool details = false)
        {
            List<Desc> items = null;
            if (commitments == null)
            {
                lock (ServiceLocator.DbContext)
                    commitments = ServiceLocator.DbContext.Commitments.Where(r => r.StartDate <= currdate).ToList();
            }

            if (currdate > DateTime.Today)
            {
                if (dsi != null)
                {
                    dsi.Percent = -1.0;
                    dsi.Label = "";
                }
            }
            else
            {
                var validCommitments = commitments.Where(r => r.StartDate <= currdate);

                var dailytodo = validCommitments.Where(r => r._type == (int)BartqHabitCallendar.Model.Commitment.RType.EveryDay);
                var dailycount = dailytodo.Count();
                var dailydone = dailytodo.Where(d => d.CommitmentDates.Any(rd => rd.Date.Value == currdate));
                var dailydonecount = dailydone.Count();

                var weeklytodo = validCommitments.Where(r => r._type == (int)BartqHabitCallendar.Model.Commitment.RType.OnceAWeek);
                var weeklycount = weeklytodo.Count();
                var firstday = currdate.AddWeek(0);
                var lastday = currdate.AddWeek(1).AddDays(-1);
                var weeklydone = weeklytodo.Where(r => r.CommitmentDates.Any(rd => rd.Date.Value >= firstday && rd.Date.Value <= lastday));
                var weeklydonecount = weeklydone.Count();

                firstday = currdate.Date.AddDays(-currdate.Day + 1);
                lastday = firstday.AddMonths(1).AddDays(-1);
                var monthlytodo = validCommitments.Where(r => r._type == (int)BartqHabitCallendar.Model.Commitment.RType.OnceAMonth);
                var monthlycount = monthlytodo.Count();
                var monthlydone = monthlytodo.Where(r => r.CommitmentDates.Any(rd => rd.Date.Value >= firstday && rd.Date.Value <= lastday));
                var monthlydonecount = monthlydone.Count();

                int total = dailycount + weeklycount + monthlycount;

                if (details)
                {
                    var _1t = string.Format("+{0:p}", 1.0 / total);
                    var done = dailydone.Concat(weeklydone).Concat(monthlydone).ToDictionary(d => d.Id);
                    items = validCommitments.Aggregate(new List<Desc>(), (acc, i) =>
                    {
                        acc.Add(new Desc()
                        {
                            Left = i.Content,
                            Right = (done.ContainsKey(i.Id) ? "☺ " + _1t : "☹")
                        });
                        return acc;
                    });
                }

                if (dsi != null)
                    if (total != 0)
                    {
                        dsi.Percent = 1.0 * (dailydonecount + weeklydonecount + monthlydonecount) / total;
                        dsi.Label = string.Format("{0}%", (int)(dsi.Percent * 100));
                    }
                    else
                    {
                        dsi.Percent = -1.0;
                        dsi.Label = "";
                    }
            }
            return items;
        }

        public void RecalculateDay(DateTime day)
        {
            Calculate(day, day);
        }

        public void RecalculateWeek(DateTime week)
        {
            var firstday = week.AddWeek(0);
            var lastday = week.AddWeek(1).AddDays(-1);

            Calculate(firstday, lastday);
        }

        public void RecalculateMonth(DateTime month)
        {
            var start = new DateTime(month.Year, month.Month, 1);
            var enddate = start.AddMonths(1).AddDays(-1);
            Calculate(start, enddate);
        }

        public static readonly Color Bad = Color.FromArgb(255, 255, 0, 0);
        public static readonly Color Good = Color.FromArgb(255, 255, 255, 255);

        public Brush Convert(DateTime dateTime, bool isSelected, Brush defaultValue, BrushType brushType)
        {
            var dsi = GetCache(dateTime.Date);
            if (brushType == BrushType.Background)
            {
                if (dsi.Percent < 0.0)
                    return new SolidColorBrush(Colors.Black);
                var left = dsi.Percent;
                double resultRed = Bad.R + left * (Good.R - Bad.R);
                double resultGreen = Bad.G + left * (Good.G - Bad.G);
                double resultBlue = Bad.B + left * (Good.B - Bad.B);
                var x = new SolidColorBrush(Color.FromArgb(255, (byte)resultRed, (byte)resultGreen, (byte)resultBlue));

                x.Opacity = isSelected ? 0.8 : 1.0;
                return x;
            }
            else
            {
                byte color = 255;
                if (dsi.Percent < 0.5)
                    color = 255;
                else
                    color = 0;
                return new SolidColorBrush(Color.FromArgb(255, color, color, color));

            }
        }
    }
}
