﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BartqHabitCallendar.Code
{

    // based on excellent Mark Monster's solution
    // http://mark.mymonster.nl/2013/07/07/make-the-listpicker-or-radlistpicker-behave-in-a-scrollviewer
    public static class ListPickerGuardian
    {
        public static void EnsureVisible(this ScrollViewer scroller, UIElement element)
        {
            scroller.UpdateLayout();

            double maxScrollPos = scroller.ExtentHeight - scroller.ViewportHeight;
            double scrollPos =
                scroller.VerticalOffset -
                scroller.TransformToVisual(element).Transform(new Point(0, 0)).Y;

            if (scrollPos > maxScrollPos) scrollPos = maxScrollPos;
            else if (scrollPos < 0) scrollPos = 0;

            scroller.ScrollToVerticalOffset(scrollPos);
        }

        public static void EnsureVisibleInScrollViewer(this UIElement element)
        {
            var scrollViewer = element.FindParentOf<ScrollViewer>();
            if (scrollViewer != null)
                scrollViewer.EnsureVisible(element);
        }

        public static T FindParentOf<T>(this UIElement element) where T : UIElement
        {
            var found = FindParentOf(element, typeof(T)) as T;
            return found;
        }

        public static DependencyObject FindParentOf(DependencyObject element, Type type)
        {
            if (type.IsInstanceOfType(element))
                return element;
            DependencyObject parent = VisualTreeHelper.GetParent(element);
            if (parent == null)
                return null;
            return FindParentOf(parent, type);
        }
    }
}
