﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Scheduler;
using BartqHabitCallendar.Model;

namespace BartqHabitCallendar.Code
{
    //utilizes portions of http://msdn.microsoft.com/en-us/library/windows/apps/hh202965(v=vs.105).aspx
    public class ReminderHelper
    {
        const string RemiderPrefix = "BHC.";
        IList<ScheduledNotification> notifications;

        public ReminderHelper()
        {
            Refresh();
        }

        public void Refresh()
        {
            notifications = ScheduledActionService.GetActions<ScheduledNotification>().ToList();
        }

        public ScheduledNotification CreateNew(DateTime remindAt, Commitment commitment)
        {
            String name = RemiderPrefix + Guid.NewGuid().ToString();
            var start = DateTime.Today.Date + remindAt.TimeOfDay;
            if (DateTime.Now > start)
                start.AddDays(1);
            RecurrenceInterval ri = RecurrenceInterval.None;
            string suffix = " ?";
            switch (commitment.Type)
            {
                case Model.Commitment.RType.EveryDay:
                    ri = RecurrenceInterval.Daily;
                    suffix = " date?";
                    break;
                case Model.Commitment.RType.OnceAWeek:
                    ri = RecurrenceInterval.Weekly;
                    suffix = " this week?";
                    break;
                case Model.Commitment.RType.OnceAMonth:
                    ri = RecurrenceInterval.EndOfMonth;
                    suffix = " this month?";
                    break;
            }
            string queryString = "?tag=" + name;
            Uri navigationUri = new Uri("/MainPage.xaml" + queryString, UriKind.Relative);
            Reminder reminder = new Reminder(name);
            reminder.Title = "Did you " + commitment.Content + suffix;
            reminder.Content = "Unfortunately you have to manually run app to mark it done. See 'about' page.";
            reminder.BeginTime = start;
            reminder.ExpirationTime = start.AddYears(100);
            reminder.RecurrenceType = ri;
            reminder.NavigationUri = navigationUri;

            // Register the reminder with the system.
            ScheduledActionService.Add(reminder);
            notifications.Add(reminder);
            return reminder;
        }

        public ScheduledNotification GetReminderFor(Commitment commitment)
        {
            return notifications.FirstOrDefault(n => n.Name == commitment.ReminderUuid);
        }

        public void RemoveNotification(ScheduledNotification notification)
        {
            ScheduledActionService.Remove(notification.Name);
            notifications.Remove(notification);
        }

        public void Reschedule(ScheduledNotification notification)
        {
            ScheduledActionService.Replace(notification);
        }

        public void ClearNotExisting(DbContext dc)
        {
            Refresh();
            var nots = notifications.ToList();
            var res = dc.Commitments.Where(r => r.ReminderUuid != null).ToDictionary(r => r.ReminderUuid);
            foreach (var not in nots)
            {
                if (!res.ContainsKey(not.Name))
                    RemoveNotification(not);
            }
        }

        public void ClearAll()
        {
            Refresh();
            var nots = notifications.ToList();
            foreach (var not in nots)
            {
                RemoveNotification(not);
            }
        }
    }
}
