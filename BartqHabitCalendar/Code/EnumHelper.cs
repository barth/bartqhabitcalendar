﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Code
{
    /// utilizes http://stackoverflow.com/a/9276348/431878 AdamCrawford answer
    public static class EnumHelper
    {
        /// <summary>
        /// Gets an attribute on an enum field value
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var enumvalstr = enumVal.ToString();
            var memInfo = type.GetMember(enumvalstr);
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            if (attributes.Length > 0)
                return (T)attributes[0];
            else throw new ArgumentException("Attribute was not found on enum " + type.Name + " value " + enumvalstr);
        }
    }
}
