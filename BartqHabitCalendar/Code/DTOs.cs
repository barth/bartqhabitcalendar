﻿using BartqHabitCallendar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO;

namespace BartqHabitCallendar.Code
{
    public class CommitmentTransferDTO
    {
        public string Content { get; set; }
        public int Type { get; set; }
        public int? Id { get; set; }
        public DateTime? Reminder { get; set; }
        public DateTime? StartDate { get; set; }
    }

    public class DateTransferDTO
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
    }

    public class CommitmentDateTransferDTO
    {
        public int CommitmentId { get; set; }
        public int DateId { get; set; }
    }

    public class FullTransferPackage : BaseTransferPackage
    {
        public List<CommitmentDateTransferDTO> CommitmentDates { get; set; }
        public List<DateTransferDTO> Dates { get; set; }

        public FullTransferPackage()
            : base()
        {
            CommitmentDates = new List<CommitmentDateTransferDTO>();
            Dates = new List<DateTransferDTO>();
        }

        public static FullTransferPackage Create(DbContext dc)
        {
            FullTransferPackage tp = new FullTransferPackage();
            int id = 0;
            Dictionary<DateTime, int> dates = new Dictionary<DateTime, int>();
            foreach (var c in dc.Commitments)
            {
                var cid = id++;
                tp.Commitments.Add(new CommitmentTransferDTO()
                {
                    Id = cid,
                    Content = c.Content,
                    Reminder = c.Reminder,
                    StartDate = c.StartDate,
                    Type = c._type
                });

                foreach (var cd in c.CommitmentDates)
                {
                    var d = cd.Date.Value;
                    int did;

                    if (!dates.ContainsKey(d))
                    {
                        did = id++;
                        tp.Dates.Add(new DateTransferDTO()
                        {
                            Id = did,
                            Date = d
                        });
                        dates[d] = did;
                    }
                    else
                        did = dates[d];

                    tp.CommitmentDates.Add(new CommitmentDateTransferDTO()
                    {
                        CommitmentId = cid,
                        DateId = did
                    });
                }
            }

            return tp;
        }

        internal void ImportTo(DbContext dbContext)
        {
            dbContext.CommitmentDates.DeleteAllOnSubmit(dbContext.CommitmentDates);
            dbContext.Dates.DeleteAllOnSubmit(dbContext.Dates);
            ServiceLocator.Reminders.ClearAll();
            dbContext.Commitments.DeleteAllOnSubmit(dbContext.Commitments);
            dbContext.SubmitChanges();

            Dictionary<int, Commitment> commitmentsCache = new Dictionary<int, Commitment>();
            Dictionary<int, Date> datesCache = new Dictionary<int, Date>();
            foreach (var date in this.Dates)
            {
                datesCache[date.Id] = new Date() { Value = date.Date };
            }
            dbContext.Dates.InsertAllOnSubmit(datesCache.Values);
            foreach (var commitment in this.Commitments)
            {
                var com = new Commitment()
                {
                    _type = commitment.Type,
                    Content = commitment.Content,
                    Reminder = commitment.Reminder,
                    StartDate = commitment.StartDate.Value
                };
                commitmentsCache[commitment.Id.Value] = com;
                if (com.Reminder != null)
                {
                    var r = ServiceLocator.Reminders.CreateNew(com.Reminder.Value, com);
                    com.ReminderUuid = r.Name;
                }

            }
            dbContext.Commitments.InsertAllOnSubmit(commitmentsCache.Values);
            dbContext.SubmitChanges();
            var comdates = new List<CommitmentDate>();
            foreach (var cd in this.CommitmentDates)
            {
                var comEntity = commitmentsCache[cd.CommitmentId];
                var dateEntity = datesCache[cd.DateId];
                var comdate = new CommitmentDate()
                {
                    CommitmentId = comEntity.Id,
                    DateId = dateEntity.Id
                };
                comdates.Add(comdate);
                comEntity.CommitmentDates.Add(comdate);
                dateEntity.CommitmentDates.Add(comdate);
            }
            dbContext.CommitmentDates.InsertAllOnSubmit(comdates);
            dbContext.SubmitChanges();

            App.ViewModel.LoadData();
            MyLiveTile.UpdateLiveTile();
            ServiceLocator.CallendarManager.RefreshCurrentMonth();
        }

        public static new FullTransferPackage Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<FullTransferPackage>(json);
        }

        public override string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class BaseTransferPackage
    {
        public List<CommitmentTransferDTO> Commitments { get; set; }

        public static BaseTransferPackage Create(DbContext dc, HashSet<int> selectedIDs)
        {
            BaseTransferPackage package = new BaseTransferPackage();
            foreach (var c in dc.Commitments.ToList().Where(com=>selectedIDs.Contains(com.Id)))
            {
                package.Commitments.Add(new CommitmentTransferDTO()
                {
                    Content = c.Content,
                    Type = c._type
                });
            }

            return package;
        }

        internal void ImportTo(DbContext dbContext)
        {
            List<Commitment> commitmentsCache = new List<Commitment>();
            foreach (var commitment in this.Commitments)
            {
                if (!dbContext.Commitments.Any(dbc => dbc.Content == commitment.Content))
                {
                    var com = new Commitment()
                    {
                        _type = commitment.Type,
                        Content = commitment.Content,
                        //Reminder = commitment.Reminder,
                        StartDate = DateTime.Today
                    };
                    //if (com.Reminder != null)
                    //{
                    //    var r = ServiceLocator.Reminders.CreateNew(com.Reminder.Value, com);
                    //    com.ReminderUuid = r.Name;
                    //}
                    commitmentsCache.Add(com);
                }
            }
            dbContext.Commitments.InsertAllOnSubmit(commitmentsCache);
            dbContext.SubmitChanges();
            
            App.ViewModel.LoadData();
            MyLiveTile.UpdateLiveTile();
            ServiceLocator.CallendarManager.RefreshCurrentMonth();
        }

        public BaseTransferPackage()
        {           
            Commitments = new List<CommitmentTransferDTO>();           
        }

        public static readonly JsonSerializerSettings JsSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public virtual string Serialize()
        {
            return JsonConvert.SerializeObject(this, JsSettings);
        }

        public static BaseTransferPackage Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<BaseTransferPackage>(json);
        }

        public const string DefaultPassword = "BartqIsTheKing";
        public const string DefaultSalt = "SaltIsSalt";

        public static byte[] Encrypt(string json, string password = null, string salt = null)
        {
            if (string.IsNullOrWhiteSpace(password))
                password = DefaultPassword;
            if (string.IsNullOrWhiteSpace(salt))
            {
                salt = DefaultSalt;
            }
            using (AesManaged aesEncryptor = new AesManaged())
            {
                // 1. AES algorith modification
                Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt), 1000);
                aesEncryptor.Key = rfc2898.GetBytes(32);
                aesEncryptor.IV = rfc2898.GetBytes(16);

                using (MemoryStream aesMemoryStream = new MemoryStream())
                {
                    using (CryptoStream aesCryptoStream = new CryptoStream(aesMemoryStream, aesEncryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        // 2. Encrypt data
                        byte[] data = Encoding.UTF8.GetBytes(json);
                        aesCryptoStream.Write(data, 0, data.Length);
                        aesCryptoStream.FlushFinalBlock();

                        // 3. convert encrypted data to base64 string
                        return aesMemoryStream.ToArray();
                    }
                }
            }
        }

        public static string Decrypt(byte[] data, string password = null, string salt = null)
        {
            string decryptedString = string.Empty;
            if (string.IsNullOrWhiteSpace(password))
                password = DefaultPassword;
            if (string.IsNullOrWhiteSpace(salt))
            {
                salt = DefaultSalt;
            }
            using (AesManaged aesDecryptor = new AesManaged())
            {
                // 1. AES algorith modification
                Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt), 1000);
                aesDecryptor.Key = rfc2898.GetBytes(32);
                aesDecryptor.IV = rfc2898.GetBytes(16);

                using (MemoryStream aesMemoryStream = new MemoryStream())
                {
                    using (CryptoStream aesCryptoStream = new CryptoStream(aesMemoryStream, aesDecryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        // 2. convert from base64 string to bytes and decrypt
                        aesCryptoStream.Write(data, 0, data.Length);
                        aesCryptoStream.FlushFinalBlock();

                        byte[] decryptBytes = aesMemoryStream.ToArray();

                        // 3. convert decrypted message to string
                        decryptedString = Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
                    }
                }
            }
            return decryptedString;
        }

        
    }
}
