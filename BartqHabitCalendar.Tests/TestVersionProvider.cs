﻿using BartqHabitCallendar.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Tests
{
    public class TestVersionProvider : VersionProvider
    {
        public override string DBVersion
        {
            get
            {
                return base.DBVersion +" test";
            }
        }

        public override string Version
        {
            get
            {
                return base.Version + " test";
            }
        }
    }
}
