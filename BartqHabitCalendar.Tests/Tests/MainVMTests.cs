﻿using BartqHabitCallendar.Model;
using BartqHabitCallendar.Test;
using BartqHabitCallendar.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Tests.Tests
{
    [TestClass]
    public class MainVMTests : BaseTestClass
    {
        protected override void Seed(DbContext dc)
        {
            #region everyday
            var today = DateTime.Today;
            Commitment evdonetoday = Commitment.CreateNew(dc,"#1 everyday done date", Commitment.RType.EveryDay);
            evdonetoday.DoneOn(dc,today);

            var yesterday = today.AddDays(-1);
            Commitment evnotdonetoday = Commitment.CreateNew(dc,"#2 everyday not done date", Commitment.RType.EveryDay);
            evnotdonetoday.DoneOn(dc,yesterday);

            Commitment evneverdone = Commitment.CreateNew(dc,"#3 everyday never done", Commitment.RType.EveryDay);
            #endregion

            #region weekly
            var mondaythisweek = today;
            while (mondaythisweek.DayOfWeek != DayOfWeek.Monday)
                mondaythisweek = mondaythisweek.AddDays(-1);
            var lastweek = mondaythisweek.AddDays(-2);

            Commitment evweeklynotdonethisweek = Commitment.CreateNew(dc,"#4 weekly not done this week", Commitment.RType.OnceAWeek);
            evweeklynotdonethisweek.DoneOn(dc, lastweek);

            Commitment evweeklydonethisweek = Commitment.CreateNew(dc,"#5 weekly done this week", Commitment.RType.OnceAWeek);
            evweeklydonethisweek.DoneOn(dc,mondaythisweek);


            Commitment weeklydonetoday = Commitment.CreateNew(dc,"#6 weekly done date", Commitment.RType.OnceAWeek);
            weeklydonetoday.DoneOn(dc,today);
            #endregion

            #region monthly
            var lastmonth = today.AddMonths(-1);
            Commitment monthlynotdonethismonth = Commitment.CreateNew(dc, "#7 monthly not done this month", Commitment.RType.OnceAMonth);
            monthlynotdonethismonth.DoneOn(dc,lastmonth);

            var thismonth = today.AddDays(-today.Day + 1);
            Commitment monthlydonethismonth = Commitment.CreateNew(dc, "#8 monthly done this month", Commitment.RType.OnceAMonth);
            monthlydonethismonth.DoneOn(dc, thismonth);

            Commitment monthlydonetoday = Commitment.CreateNew(dc, "#9 monthly done this date", Commitment.RType.OnceAMonth);
            monthlydonetoday.DoneOn(dc, today);
            #endregion
        }

        [TestMethod]
        public void WeeklyCommitmentsForToday()
        {
            //given
            var everyday = db.Commitments
                .Where(r => r._type == (int)(Commitment.RType.OnceAWeek))
                .ToList();
            //when
            var fortoday = everyday
                .Where(r => MainViewModel.ForToday(r))
                .ToList();
            //then
            Assert.IsTrue(fortoday.Count == 2);
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#4")));
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#6")));
        }

        [TestMethod]
        public void MonthlyCommitmentsForToday()
        {
            //given
            var everyday = db.Commitments
                .Where(r => r._type == (int)(Commitment.RType.OnceAMonth))
                .ToList();
            //when
            var fortoday = everyday
                .Where(r => MainViewModel.ForToday(r))
                .ToList();
            //then
            Assert.IsTrue(fortoday.Count == 2);
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#7")));
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#9")));
        }

        [TestMethod]
        public void EverydayCommitmentsForToday()
        {
            //given
            var everyday = db.Commitments
                .Where(r => r._type == (int)(Commitment.RType.EveryDay))
                .ToList();
            //when
            var fortoday = everyday
                .Where(r => MainViewModel.ForToday(r))
                .ToList();
            //then
            Assert.IsTrue(fortoday.Count == 3);
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#1")));
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#2")));
            Assert.IsTrue(fortoday.Any(r => r.Content.StartsWith("#3")));
        }

        [TestMethod]
        public void MainVMTodayDoneTest()
        {
            //given
            MainViewModel mvm = new MainViewModel();
            //when
            mvm.LoadData(db);
            var done = mvm.TodayItems.Where(td => td.Done).ToList();
            var commitmentsDoneToday = db.Commitments
                .Where(r => r.CommitmentDates.Any(rd => rd.Date.Value == DateTime.Today))
                .ToList();
            //then
            Assert.IsTrue(mvm.IsDataLoaded);
            Assert.IsTrue(done.Count == commitmentsDoneToday.Count);
            Assert.IsTrue(done.All(d=>commitmentsDoneToday.Any(rd => rd.Id==d.Model.Id)));
        }

        [TestMethod]
        public void MainVMTodayImageSource()
        {
            //given
            MainViewModel mvm = new MainViewModel();
            //when
            mvm.LoadData(db);
            //then
            var ok = true;
            foreach (var item in mvm.TodayItems)
            {
                ok &= (item.Done && item.ImageUri.Contains("check")) 
                    || (!item.Done && !item.ImageUri.Contains("check"));
            }
            Assert.IsTrue(ok);
        }
    }
}
