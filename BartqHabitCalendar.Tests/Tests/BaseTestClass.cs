﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using BartqHabitCallendar.Test;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Tests.Tests
{
    public class BaseTestClass
    {
        protected SeededDbContext db;

        [TestInitialize]
        public virtual void StartUp()
        {
            db = new SeededDbContext(Seed, true);
            ServiceLocator.SetDbContext(db);
            ServiceLocator.SetNavigationService(new TestNavigationService());
        }

        protected virtual void Seed(DbContext dc) { }

        [TestCleanup]
        public virtual void TearDown()
        {
            db.Dispose();
        }
    }
}
