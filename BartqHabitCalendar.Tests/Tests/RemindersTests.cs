﻿using BartqHabitCallendar.Code;
using BartqHabitCallendar.Model;
using BartqHabitCallendar.Test;
using BartqHabitCallendar.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Tests.Tests
{
    [TestClass]
    public class RemindersTests : BaseTestClass
    {
        public override void StartUp()
        {
            base.StartUp();
            ServiceLocator.Reminders.ClearAll();
        }

        public override void TearDown()
        {
            base.TearDown();
            ServiceLocator.Reminders.ClearAll();
        }

        const string Text = "Test text";

        [TestMethod]
        public void AddNew()
        {
            //given            
            ServiceLocator.NavigationService.NavigateTo(new Uri("/AddNew.xaml", UriKind.Relative));
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            //when
            model = DbSeeders.AddNewCommitment(text);
            //then
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.Content == text);
            Assert.IsNotNull(res);
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsTrue(rem.Title.Contains(model.CommitmentText));
        }

        [TestMethod]
        public void Remove()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new CommitmentViewModel(res);
            rvm.RemoveCommitment = new RemoveCommitmentTestCommand();
            //when
            rvm.RemoveCommitment.Execute(rvm);
            //then
            Assert.IsNull(ServiceLocator.Reminders.GetReminderFor(res));
        }

        [TestMethod]
        public void DoneToday()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var tommorow = DateTime.Today.AddDays(1).Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, tommorow);
        }

        [TestMethod]
        public void UnDoneToday()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var today = DateTime.Today.Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, today);
        }

        [TestMethod]
        public void DoneThisWeek()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text, Commitment.RType.OnceAWeek);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var nextweeklastday = DateTime.Today.AddWeek(2).AddDays(-1).Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, nextweeklastday);
        }

        [TestMethod]
        public void UnDoneThisWeek()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text, Commitment.RType.OnceAWeek);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var thisweeklastday = DateTime.Today.AddWeek(1).AddDays(-1).Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, thisweeklastday);
        }

        [TestMethod]
        public void DoneThisMonth()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text, Commitment.RType.OnceAMonth);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var nextmonthlastday = DateTime.Today.AddDays(-DateTime.Today.Day+1)
                .AddMonths(2).AddDays(-1).Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, nextmonthlastday);
        }

        [TestMethod]
        public void UnDoneThisMonth()
        {
            //given
            string text = Text + Guid.NewGuid().ToString();
            AddNewViewModel model;
            model = DbSeeders.AddNewCommitment(text, Commitment.RType.OnceAWeek);
            var res = ServiceLocator.DbContext.Commitments.FirstOrDefault(r => r.ReminderUuid != null);
            Assert.IsNotNull(res);
            var rvm = new TodayCommitmentViewModel(res);
            rvm.MarkDoneCommand = new MarkCommitmentDone();
            //when
            rvm.MarkDoneCommand.Execute(rvm);
            rvm.MarkDoneCommand.Execute(rvm);
            //then
            var rem = ServiceLocator.Reminders.GetReminderFor(res);
            Assert.IsNotNull(rem);
            Assert.IsNotNull(res.Reminder);
            var thismonthlastday = DateTime.Today.AddDays(-DateTime.Today.Day + 1)
                .AddMonths(1).AddDays(-1).Add(res.Reminder.Value.TimeOfDay);
            Assert.Equals(rem.BeginTime, thismonthlastday);
        }
    }

    public class RemoveCommitmentTestCommand : RemoveCommitmentCommand
    {
        protected override bool Confirm()
        {
            return true;
        }
    }
}
