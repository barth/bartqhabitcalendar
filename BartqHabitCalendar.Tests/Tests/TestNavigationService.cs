﻿using BartqHabitCallendar.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqHabitCallendar.Tests.Tests
{
    public class TestNavigationService : INavigationService
    {
        public Stack<Uri> Trace { get; private set; }

        public event System.Windows.Navigation.NavigatingCancelEventHandler Navigating;

        public void NavigateTo(Uri pageUri)
        {
            if(Trace.Count==0 || pageUri!=Trace.Peek())
                Trace.Push(pageUri);
        }

        public void GoBack()
        {
            Trace.Pop();
        }

        public TestNavigationService()
        {
            Trace = new Stack<Uri>();
            NavigateTo(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}
