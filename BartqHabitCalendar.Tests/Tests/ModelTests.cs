﻿using System;
using BartqHabitCallendar.Model;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BartqHabitCallendar.Test;

namespace BartqHabitCallendar.Tests.Tests
{
    [TestClass]
    public class ModelTests : BaseTestClass
    {
        public const string TestContent = "work on BartqHabitCalendar";

        protected override void Seed(DbContext dc)
        {
            Commitment r = new Commitment()
            {
                Content = TestContent,
                Type = Commitment.RType.EveryDay
            };
            dc.Commitments.InsertOnSubmit(r);
            dc.SubmitChanges();

            Date d = new Date()
            {
                Value = DateTime.Today
            };

            dc.Dates.InsertOnSubmit(d);
            dc.SubmitChanges();

            CommitmentDate rd = new CommitmentDate()
            {
                DateId = d.Id,
                CommitmentId = r.Id
            };

            dc.CommitmentDates.InsertOnSubmit(rd);
            r.CommitmentDates.Add(rd);
            d.CommitmentDates.Add(rd);
            dc.SubmitChanges();
        }

        [TestMethod]
        public void CommitmentExists()
        {
            //when
            var r = db.Commitments.Where(rs => rs._type == (int)Commitment.RType.EveryDay).FirstOrDefault();
            //then
            Assert.IsNotNull(r);
            Assert.IsTrue(r.Content==TestContent);
        }

        [TestMethod]
        public void CommitmentDoneToday()
        {
            //when
            var r = db.Commitments.Where(rs => rs._type == (int)Commitment.RType.EveryDay).FirstOrDefault();
            var rd = r.CommitmentDates.FirstOrDefault();
            var d = rd.Date;
            //then
            Assert.IsNotNull(d);
            Assert.IsTrue(d.Value == DateTime.Today);
        }
    }
}
